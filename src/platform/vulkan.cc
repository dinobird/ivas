#define VMA_DEBUG_MARGIN 16
#define VMA_DEBUG_DETECT_CORRUPTION 1
#define VMA_IMPLEMENTATION
#include <vma/vk_mem_alloc.h>

inline constexpr int imax(int a, int b){return a>b?a:b;}
inline constexpr int imin(int a, int b){return a<b?a:b;}

#if 1
#define IVAS_DEBUG(IVAS_DEBUG) do{IVAS_DEBUG}while(false)
#else
#define IVAS_DEBUG(IVAS_DEBUG) do{}while(false)
#endif

std::vector<u32> readFile32(char const * const filename){
    FILE* file = fopen(filename, "rb");
    if(!file){
        printf("Could not read %s\n", filename);
        return std::vector<u32>();
    } 
    fseek(file, 0, SEEK_END);
    int elements = ftell(file)/sizeof(u32);
    rewind(file);
    std::vector<u32> ret(elements);
    int bytes_read = fread(&ret[0],sizeof(u32),elements,file);
    if(bytes_read!=elements){
        printf("failed to read %s\n", filename);
        return std::vector<u32>();
    } 
    fclose(file);
    return ret;
}

constexpr char const * interpret_vk_result(VkResult const x){ 
    switch(x){
        case VK_SUCCESS:                        return "success";
        case VK_NOT_READY:                      return "not ready";
        case VK_TIMEOUT:                        return "timeout";
        case VK_EVENT_SET:                      return "event set";
        case VK_EVENT_RESET:                    return "event reset";
        case VK_INCOMPLETE:                     return "incomplete";
        case VK_ERROR_OUT_OF_HOST_MEMORY:       return "out of host memory";
        case VK_ERROR_OUT_OF_DEVICE_MEMORY:     return "out of device memory";
        case VK_ERROR_INITIALIZATION_FAILED:    return "initialization failed";
        case VK_ERROR_DEVICE_LOST:              return "device lost";
        case VK_ERROR_LAYER_NOT_PRESENT:        return "layer not present";
        case VK_ERROR_EXTENSION_NOT_PRESENT:    return "extension not present";
        case VK_ERROR_FEATURE_NOT_PRESENT:      return "feature not present";
        case VK_ERROR_INCOMPATIBLE_DRIVER:      return "incompatible driver";
        case VK_ERROR_TOO_MANY_OBJECTS:         return "too many objects";
        case VK_ERROR_FORMAT_NOT_SUPPORTED:     return "format not supported";
        case VK_ERROR_FRAGMENTED_POOL:          return "fragmented pool";
        case VK_ERROR_OUT_OF_POOL_MEMORY:       return "out of pool memory";
        case VK_ERROR_INVALID_EXTERNAL_HANDLE:  return "invalid external handle";
        case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: return "[khr] incompatible display";
        case VK_ERROR_SURFACE_LOST_KHR:         return "[khr] surface lost";
        case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: return "[khr] native window in use";        
        case VK_ERROR_INVALID_SHADER_NV:        return "[nv] invalid shader";
        default:                                return "unknown vulkan error (likely an extension)";
    }
};

constexpr char const * interpret_vk_debug_report_object_type(VkDebugReportObjectTypeEXT const x){ 
    switch(x){
        case VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT: return "unknown type";
        case VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT: return "instance";
        case VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT: return "physical device";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT: return "device";
        case VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT: return "queue";
        case VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT: return "semaphore";
        case VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT: return "command buffer";
        case VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT: return "fence";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT: return "device memory";
        case VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT: return "buffer";
        case VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT: return "image";
        case VK_DEBUG_REPORT_OBJECT_TYPE_EVENT_EXT: return "event";
        case VK_DEBUG_REPORT_OBJECT_TYPE_QUERY_POOL_EXT: return "query pool";
        case VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_VIEW_EXT: return "buffer view";
        case VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT: return "image view";
        case VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT: return "shader module";
        case VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_CACHE_EXT: return "pipeline cache";
        case VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT: return "pipeline layout";
        case VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT: return "render pass";
        case VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT: return "pipeline";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT: return "descriptor set layout";
        case VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT: return "sampler";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT: return "descriptor pool";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT: return "descriptor set";
        case VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT: return "framebuffer ";
        case VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT: return "command pool";
        case VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT: return "[khr] surface";
        case VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT: return "[khr] swapchain";
        case VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_EXT: return "debug report";
        default:                                            return "unrecognized type (likely an extension)";
    }
}

constexpr VkResult vulkanCrashOnError(VkResult const result, char const * const msg, bool printOnSuccess = false){
    if(result<0){ 
        printf("%s: %s\n", interpret_vk_result(result), msg);
        exit(1);
    } else {
        if(printOnSuccess && result != VK_SUCCESS)
        {
            printf("%s: %s\n", interpret_vk_result(result), msg);
        }
        return result;
    }
    assert(false);
}


//  /$$    /$$           /$$ /$$                                 /$$   /$$                                                                                    
// | $$   | $$          | $$| $$                                | $$$ | $$                                                                                    
// | $$   | $$ /$$   /$$| $$| $$   /$$  /$$$$$$  /$$$$$$$       | $$$$| $$  /$$$$$$  /$$$$$$/$$$$   /$$$$$$   /$$$$$$$  /$$$$$$   /$$$$$$   /$$$$$$$  /$$$$$$ 
// |  $$ / $$/| $$  | $$| $$| $$  /$$/ |____  $$| $$__  $$      | $$ $$ $$ |____  $$| $$_  $$_  $$ /$$__  $$ /$$_____/ /$$__  $$ |____  $$ /$$_____/ /$$__  $$
//  \  $$ $$/ | $$  | $$| $$| $$$$$$/   /$$$$$$$| $$  \ $$      | $$  $$$$  /$$$$$$$| $$ \ $$ \ $$| $$$$$$$$|  $$$$$$ | $$  \ $$  /$$$$$$$| $$      | $$$$$$$$
//   \  $$$/  | $$  | $$| $$| $$_  $$  /$$__  $$| $$  | $$      | $$\  $$$ /$$__  $$| $$ | $$ | $$| $$_____/ \____  $$| $$  | $$ /$$__  $$| $$      | $$_____/
//    \  $/   |  $$$$$$/| $$| $$ \  $$|  $$$$$$$| $$  | $$      | $$ \  $$|  $$$$$$$| $$ | $$ | $$|  $$$$$$$ /$$$$$$$/| $$$$$$$/|  $$$$$$$|  $$$$$$$|  $$$$$$$
//     \_/     \______/ |__/|__/  \__/ \_______/|__/  |__/      |__/  \__/ \_______/|__/ |__/ |__/ \_______/|_______/ | $$____/  \_______/ \_______/ \_______/
//                                                                                                                    | $$                                    
//                                                                                                                    | $$                                    
//                                                                                                                    |__/                                    
namespace vulkan
{
//    SSSSSSSSSSSSSSS      tttt                                                                            tttt                           
//  SS:::::::::::::::S  ttt:::t                                                                         ttt:::t                           
// S:::::SSSSSS::::::S  t:::::t                                                                         t:::::t                           
// S:::::S     SSSSSSS  t:::::t                                                                         t:::::t                           
// S:::::S        ttttttt:::::ttttttt   rrrrr   rrrrrrrrr   uuuuuu    uuuuuu      ccccccccccccccccttttttt:::::ttttttt        ssssssssss   
// S:::::S        t:::::::::::::::::t   r::::rrr:::::::::r  u::::u    u::::u    cc:::::::::::::::ct:::::::::::::::::t      ss::::::::::s  
//  S::::SSSS     t:::::::::::::::::t   r:::::::::::::::::r u::::u    u::::u   c:::::::::::::::::ct:::::::::::::::::t    ss:::::::::::::s 
//   SS::::::SSSSStttttt:::::::tttttt   rr::::::rrrrr::::::ru::::u    u::::u  c:::::::cccccc:::::ctttttt:::::::tttttt    s::::::ssss:::::s
//     SSS::::::::SS    t:::::t          r:::::r     r:::::ru::::u    u::::u  c::::::c     ccccccc      t:::::t           s:::::s  ssssss 
//        SSSSSS::::S   t:::::t          r:::::r     rrrrrrru::::u    u::::u  c:::::c                   t:::::t             s::::::s      
//             S:::::S  t:::::t          r:::::r            u::::u    u::::u  c:::::c                   t:::::t                s::::::s   
//             S:::::S  t:::::t    ttttttr:::::r            u:::::uuuu:::::u  c::::::c     ccccccc      t:::::t    ttttttssssss   s:::::s 
// SSSSSSS     S:::::S  t::::::tttt:::::tr:::::r            u:::::::::::::::uuc:::::::cccccc:::::c      t::::::tttt:::::ts:::::ssss::::::s
// S::::::SSSSSS:::::S  tt::::::::::::::tr:::::r             u:::::::::::::::u c:::::::::::::::::c      tt::::::::::::::ts::::::::::::::s 
// S:::::::::::::::SS     tt:::::::::::ttr:::::r              uu::::::::uu:::u  cc:::::::::::::::c        tt:::::::::::tt s:::::::::::ss  
//  SSSSSSSSSSSSSSS         ttttttttttt  rrrrrrr                uuuuuuuu  uuuu    cccccccccccccccc          ttttttttttt    sssssssssss    
                                                                                                                                       
    struct VulkanDevice{
        u32 graphicsQueueIndex;
        u32 graphicsQueueFamilyIndex;

        u32 computeQueueIndex;
        u32 computeQueueFamilyIndex;

        VkQueueFamilyProperties queueFamilyProperties;
        VkPhysicalDevice physicalDevice;
        VkDevice device;
        VmaAllocator allocator;

        VkQueue graphicsQueue;
        VkQueue computeQueue;
        std::vector<u32> graphicsQueueFamilyIndices;
        std::vector<u32> computeQueueFamilyIndices;
        std::vector<VkDeviceQueueCreateInfo> deviceQueueCreateInfos;
        std::vector<VkDeviceQueueCreateInfo> usedDeviceQueueCreateInfos;
    };

    struct VulkanInstance {
        VkInstance instance;
        std::vector<char const*> enabledExtensions;
        u32 enabledExtensionCount;
    };

    struct VulkanSwapchain
    {
        VkSwapchainCreateInfoKHR swapchainCreateInfo;
        VkSwapchainKHR swapchain;
        VkSurfaceFormatKHR surfaceFormat;
        std::vector<VkImage> swapchainImages;
        std::vector<VkImageView> swapchainImageViews;
        std::vector<VkImageViewCreateInfo> swapchainImageViewCreateInfos;
    };

    struct VulkanCommandPool
    {
        VkCommandPoolCreateInfo commandPoolCreateInfo;
        VkCommandPool commandPool;
        
        VkCommandBuffer availablePrimaryCommandBuffer;
        VkCommandBuffer availableSecondaryCommandBuffer;
        VkCommandBuffer availableCopyCommandBuffer;

        VkCommandBuffer inUsePrimaryCommandBuffer;
        VkCommandBuffer inUseSecondaryCommandBuffer;
        VkCommandBuffer inUseCopyCommandBuffer;
    };
    
    struct VulkanShaderModule{
        VkShaderModuleCreateInfo shaderModuleCreateInfo;
        VkShaderModule shaderModule;
        VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfo;
    };

    struct VulkanGraphicsPipeline{
        VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo;
        VkPipeline pipeline;
        VkShaderStageFlags shaderStages;
    };

    struct VulkanImageView {
        VkImageView imageView;
        VkImageViewCreateInfo imageViewCreateInfo;
    };

    struct VulkanDepthStencilImage {
        VmaAllocation allocation;
        VmaAllocationCreateInfo allocationCreateInfo;
        VmaAllocationInfo allocationInfo;

        // VkShaderStageFlags shaderStageFlags;
        // VkDescriptorType descriptorType;
        // VkDescriptorImageInfo descriptorImageInfo;

        VkImage image;
        VkImageCreateInfo imageCreateInfo;

        VulkanImageView depthImageView;
        VulkanImageView stencilImageView;
        VulkanImageView depthStencilImageView;
    };
    
    struct VulkanImage {
        VmaAllocation allocation;
        VmaAllocationCreateInfo allocationCreateInfo;
        VmaAllocationInfo allocationInfo;

        // VkShaderStageFlags shaderStageFlags;
        // VkDescriptorType descriptorType;
        // VkDescriptorImageInfo descriptorImageInfo;

        VkImage image;
        VkImageCreateInfo imageCreateInfo;
        std::vector<VulkanImageView> imageViews;
    };


    struct VulkanBuffer {
        VkBuffer buffer;
        VmaAllocation bufferAllocation;
        VkDescriptorBufferInfo bufferInfo;

        VkBufferCreateInfo bufferCreateInfo;
        VmaAllocationCreateInfo bufferAllocationCreateInfo;

        VmaAllocationInfo bufferAllocationInfo;

        VkShaderStageFlags shaderStageFlags;
        VkDescriptorType descriptorType;
    };

    struct VulkanDescriptorSet
    {
        VkDescriptorPool pool;
        VkDescriptorPoolCreateInfo poolCreateInfo;
        VkDescriptorSetLayout layout;
        VkDescriptorSetLayoutCreateInfo layoutCreateInfo;
        std::vector<VkDescriptorSetLayoutBinding> layoutBindings;
        std::vector<VkDescriptorSetAllocateInfo> setAllocateInfos;
        std::vector<VkDescriptorPoolSize> poolSizes;
        std::vector<VkDescriptorSet> descriptorSets;
        std::vector<VkWriteDescriptorSet> writes;
    };

    struct VulkanVertexBuffer{
        VkBuffer buffer;
        VmaAllocation allocation;
        VmaAllocationInfo allocationInfo;

        VkBufferCreateInfo bufferCreateInfo;
        VkVertexInputBindingDescription inputBindingDescription;
        VmaAllocationCreateInfo allocationCreateInfo;

        std::vector<VkVertexInputAttributeDescription> attributeDescriptions;
    };

    struct VulkanTestImplementation {
        GLFWwindow* window;
        VulkanInstance instance;
        VkSurfaceKHR surface;
        VulkanDevice device;
        VulkanSwapchain swapchain;
        VulkanCommandPool commandPool;
        VkRenderPass renderPass;
        std::vector<VkFramebuffer> framebuffers;
        VkSemaphore imageAvailable;
        VkSemaphore renderingFinished;
        VkFence completedQueueExecution;
    };

                                                                                                                                       
//    SSSSSSSSSSSSSSS      tttt                                                                            tttt                           
//  SS:::::::::::::::S  ttt:::t                                                                         ttt:::t                           
// S:::::SSSSSS::::::S  t:::::t                                                                         t:::::t                           
// S:::::S     SSSSSSS  t:::::t                                                                         t:::::t                           
// S:::::S        ttttttt:::::ttttttt   rrrrr   rrrrrrrrr   uuuuuu    uuuuuu      ccccccccccccccccttttttt:::::ttttttt        ssssssssss   
// S:::::S        t:::::::::::::::::t   r::::rrr:::::::::r  u::::u    u::::u    cc:::::::::::::::ct:::::::::::::::::t      ss::::::::::s  
//  S::::SSSS     t:::::::::::::::::t   r:::::::::::::::::r u::::u    u::::u   c:::::::::::::::::ct:::::::::::::::::t    ss:::::::::::::s 
//   SS::::::SSSSStttttt:::::::tttttt   rr::::::rrrrr::::::ru::::u    u::::u  c:::::::cccccc:::::ctttttt:::::::tttttt    s::::::ssss:::::s
//     SSS::::::::SS    t:::::t          r:::::r     r:::::ru::::u    u::::u  c::::::c     ccccccc      t:::::t           s:::::s  ssssss 
//        SSSSSS::::S   t:::::t          r:::::r     rrrrrrru::::u    u::::u  c:::::c                   t:::::t             s::::::s      
//             S:::::S  t:::::t          r:::::r            u::::u    u::::u  c:::::c                   t:::::t                s::::::s   
//             S:::::S  t:::::t    ttttttr:::::r            u:::::uuuu:::::u  c::::::c     ccccccc      t:::::t    ttttttssssss   s:::::s 
// SSSSSSS     S:::::S  t::::::tttt:::::tr:::::r            u:::::::::::::::uuc:::::::cccccc:::::c      t::::::tttt:::::ts:::::ssss::::::s
// S::::::SSSSSS:::::S  tt::::::::::::::tr:::::r             u:::::::::::::::u c:::::::::::::::::c      tt::::::::::::::ts::::::::::::::s 
// S:::::::::::::::SS     tt:::::::::::ttr:::::r              uu::::::::uu:::u  cc:::::::::::::::c        tt:::::::::::tt s:::::::::::ss  
//  SSSSSSSSSSSSSSS         ttttttttttt  rrrrrrr                uuuuuuuu  uuuu    cccccccccccccccc          ttttttttttt    sssssssssss    
                                                                                                                                       
    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugReportFlagsEXT /*flags*/,
        VkDebugReportObjectTypeEXT objType,
        u64 obj,
        size_t location,
        i32 code,
        const char* layerPrefix,
        const char* msg,
        void* userData)
    {
        (void)userData;
        //SDL_LogPriority const severity = flags&VK_DEBUG_REPORT_ERROR_BIT_EXT?               SDL_LOG_PRIORITY_ERROR
        //                               : flags&VK_DEBUG_REPORT_WARNING_BIT_EXT?             SDL_LOG_PRIORITY_WARN
        //                               : flags&VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT? SDL_LOG_PRIORITY_WARN
        //                               : flags&VK_DEBUG_REPORT_DEBUG_BIT_EXT?               SDL_LOG_PRIORITY_DEBUG
        //                               : flags&VK_DEBUG_REPORT_INFORMATION_BIT_EXT?         SDL_LOG_PRIORITY_INFO
        //                               : SDL_LOG_PRIORITY_CRITICAL; // none of the bits set

        //SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, severity, "%s (type: %s) %s (obj:%ld loc:%zu code:%d)", layerPrefix, interpret_vk_debug_report_object_type(objType),  msg, obj, location, code);
        printf("%s (type: %s) %s (obj:%llu loc:%zu code:%d)\n", layerPrefix, interpret_vk_debug_report_object_type(objType),  msg, obj, location, code);

        return VK_FALSE; // should abort? spec recommends always returning false
    }

    VkResult createDebugReportCallbackEXT(VkInstance instance,
        VkDebugReportCallbackCreateInfoEXT const * pCreateInfo,
        VkAllocationCallbacks const * pAllocator,
        VkDebugReportCallbackEXT* pCallback)
    {
        auto func = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
        if (func != NULL) 
        {
            return func(instance, pCreateInfo, pAllocator, pCallback);
        }
        else 
        {
            printf("VK_ERROR_EXTENSION_NOT_PRESENT\n");
            return VK_ERROR_EXTENSION_NOT_PRESENT;
        }
    }

    VkDebugReportCallbackEXT setupDebugCallback(VkInstance instance)
    {
        VkDebugReportCallbackCreateInfoEXT createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
        createInfo.flags = VK_DEBUG_REPORT_INFORMATION_BIT_EXT
            | VK_DEBUG_REPORT_WARNING_BIT_EXT
            | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT
            | VK_DEBUG_REPORT_ERROR_BIT_EXT
            | VK_DEBUG_REPORT_DEBUG_BIT_EXT
            ;
        createInfo.pfnCallback = debugCallback;
        VkDebugReportCallbackEXT callback;
        if (createDebugReportCallbackEXT(instance, &createInfo, NULL, &callback) != VK_SUCCESS) 
        {
            printf("Failed to register Vulkan Debug callback!\n");
        }
        else
        {
            printf("Registered Vulkan Debug callback!\n");
        }
        return callback;
    }

    GLFWwindow* createGLFWwindow(i32 width, i32 height)
    {
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        auto window = glfwCreateWindow(width, height, "Ivas (Vulkan)", nullptr, nullptr);
        if(!window)
        {
            glfwTerminate();
            vulkanCrashOnError(VK_ERROR_INITIALIZATION_FAILED, "Cannot create GLFW Window");
            return nullptr;
        }
        return window;
    }

    void copyWithStagingBuffer(VulkanDevice device, VulkanCommandPool commandPool, VkBuffer bufferTarget, u32 size, void* data)
    {
        VkBufferCreateInfo stagingBufferCreateInfo = 
        {
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .size = size,
            .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
            .queueFamilyIndexCount = 1,
            .pQueueFamilyIndices = &device.usedDeviceQueueCreateInfos[0].queueFamilyIndex
        };

        VmaAllocationCreateInfo stagingAllocationCreateInfo = {
            .usage = VMA_MEMORY_USAGE_CPU_TO_GPU,
            .flags = VMA_ALLOCATION_CREATE_MAPPED_BIT
        };

        VkBuffer stagingBuffer;
        VmaAllocation stagingAllocation;
        VmaAllocationInfo stagingAllocationInfo;
        vmaCreateBuffer(
            device.allocator, 
            &stagingBufferCreateInfo, 
            &stagingAllocationCreateInfo, 
            &stagingBuffer, 
            &stagingAllocation, 
            &stagingAllocationInfo 
        );

        memcpy(stagingAllocationInfo.pMappedData, data, size);

        VkBufferCopy copyInfo =
        {
            .size = stagingAllocationInfo.size
        };

        VkCommandBufferBeginInfo cmdBeginInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
        };
        vkBeginCommandBuffer(commandPool.availablePrimaryCommandBuffer, &cmdBeginInfo);
        vkCmdCopyBuffer     (commandPool.availablePrimaryCommandBuffer, stagingBuffer, bufferTarget, 1, &copyInfo);
        vkEndCommandBuffer  (commandPool.availablePrimaryCommandBuffer);

        VkSubmitInfo copyCmdSubmitInfo = 
        {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .commandBufferCount = 1,
            .pCommandBuffers = &commandPool.availablePrimaryCommandBuffer
        };

        vkQueueSubmit(device.graphicsQueue, 1, &copyCmdSubmitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(device.graphicsQueue);

        vmaDestroyBuffer(device.allocator, stagingBuffer, stagingAllocation);
    }

    bool presentModeAvailable(VkPresentModeKHR requestedPresentMode, std::vector<VkPresentModeKHR> availablePresentModes)
    {
        for(auto const& availableMode : availablePresentModes){
            if(availableMode == requestedPresentMode)
            {
                return true;
            }
        }
        return false;
    }

    bool isFormatSupportedWithFeatures(
        VulkanDevice device,
        VkFormat formatRequested,
        VkFormatFeatureFlags linearTilingFeatures,
        VkFormatFeatureFlags optimalTilingFeatures,
        VkFormatFeatureFlags bufferFeatures)
    {
        
        VkFormatProperties formatProperties;
        vkGetPhysicalDeviceFormatProperties(device.physicalDevice, formatRequested, &formatProperties);

        return (
            ((linearTilingFeatures & formatProperties.linearTilingFeatures) == linearTilingFeatures) &&
            ((optimalTilingFeatures & formatProperties.optimalTilingFeatures) == optimalTilingFeatures) &&
            ((bufferFeatures & formatProperties.bufferFeatures) == bufferFeatures)
            );
    }
    
    VkFormat findDepthStencilFormat(VulkanDevice device)
    {
        return
            isFormatSupportedWithFeatures(device, VK_FORMAT_D24_UNORM_S8_UINT,      0, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT, 0) ? VK_FORMAT_D24_UNORM_S8_UINT :
            isFormatSupportedWithFeatures(device, VK_FORMAT_X8_D24_UNORM_PACK32,    0, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT, 0) ? VK_FORMAT_X8_D24_UNORM_PACK32 :
            isFormatSupportedWithFeatures(device, VK_FORMAT_D32_SFLOAT,             0, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT, 0) ? VK_FORMAT_D32_SFLOAT :
            isFormatSupportedWithFeatures(device, VK_FORMAT_D32_SFLOAT_S8_UINT,     0, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT, 0) ? VK_FORMAT_D32_SFLOAT_S8_UINT :
            isFormatSupportedWithFeatures(device, VK_FORMAT_D16_UNORM_S8_UINT,      0, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT, 0) ? VK_FORMAT_D16_UNORM_S8_UINT :
            isFormatSupportedWithFeatures(device, VK_FORMAT_D16_UNORM,              0, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT, 0) ? VK_FORMAT_D16_UNORM :
            VK_FORMAT_UNDEFINED;
    }

    VulkanInstance createVulkanInstance()
    {
        u32 instanceLayerPropertyCount = 0;
        vkEnumerateInstanceLayerProperties(&instanceLayerPropertyCount, nullptr);
        VkLayerProperties instanceLayerProperties[instanceLayerPropertyCount];
        vkEnumerateInstanceLayerProperties(&instanceLayerPropertyCount, &instanceLayerProperties[0]);

        u32 glfwRequiredInstanceExtensionCount = 0;
        const char** glfwRequiredInstanceExtensions = glfwGetRequiredInstanceExtensions(&glfwRequiredInstanceExtensionCount);

        std::vector<char const*> enabledLayerNames;

        IVAS_DEBUG(
        printf("All InstanceLayerProperties :\n");
        for(auto const& instanceLayerProperty : instanceLayerProperties)
        {
            printf("%s\n", instanceLayerProperty.layerName);
            enabledLayerNames.push_back(instanceLayerProperty.layerName);
        }
        );

        std::vector<char const *> instanceExtensionProperties(glfwRequiredInstanceExtensions, &glfwRequiredInstanceExtensions[glfwRequiredInstanceExtensionCount]);
        instanceExtensionProperties.push_back("VK_EXT_debug_report");

        IVAS_DEBUG(
        printf("All Extensions :\n");
        for(int i=0; i< glfwRequiredInstanceExtensionCount;i++)
        {
            printf("%s\n", instanceExtensionProperties[i]);
        }
        );

        std::vector<char const *> coreValidationLayers = { 
            "VK_LAYER_LUNARG_standard_validation"
        };

        VkApplicationInfo applicationInfo = {
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pApplicationName = "Vulkan Testbed",
            .applicationVersion = 0,
            .pEngineName = "Ivas",
            .engineVersion = 0,
            .apiVersion = VK_API_VERSION_1_0
        };

        VkInstanceCreateInfo instanceCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            .pApplicationInfo = &applicationInfo,
            .enabledLayerCount = coreValidationLayers.size(),
            .ppEnabledLayerNames = &coreValidationLayers[0],
            .enabledExtensionCount = instanceExtensionProperties.size(),
            .ppEnabledExtensionNames = &instanceExtensionProperties[0]
        };
        VkInstance instance;
        vulkanCrashOnError(vkCreateInstance(&instanceCreateInfo, nullptr, &instance), "Creating Vulkan instance");

        IVAS_DEBUG(setupDebugCallback(instance););

        return {
            .instance = instance,
            .enabledExtensions = instanceExtensionProperties
        };
    }

    VkSurfaceKHR createVulkanSurface(GLFWwindow* window, VkInstance instance)
    {
        VkSurfaceKHR surface;
        vulkanCrashOnError(
            glfwCreateWindowSurface(
                instance,
                window,
                nullptr,
                &surface
            ),
            "Creating Surface!"
        );

        return surface;
    }

    VulkanDevice createVulkanDevice(VulkanInstance instance, VkSurfaceKHR surface)
    {
        // Physical & Logical Device Creation
        u32 physicalDeviceCount = 0;
        vkEnumeratePhysicalDevices(instance.instance, &physicalDeviceCount, nullptr);
        std::vector<VkPhysicalDevice> physicalDevices;
        physicalDevices.resize(physicalDeviceCount);
        vkEnumeratePhysicalDevices(instance.instance, &physicalDeviceCount, &physicalDevices[0]);

        std::vector<VulkanDevice> vulkanDevices;
        std::vector<float> queuePriorities(256, 1.0f);

        u32 physicalDeviceIndex = 0;
        for(const auto& physicalDevice : physicalDevices)
        {
            VkPhysicalDeviceProperties physicalDeviceProperties;
            vkGetPhysicalDeviceProperties(physicalDevice, &physicalDeviceProperties);
            printf("%s\n", physicalDeviceProperties.deviceName);

            u32 queueFamilyPropertyCount = 0;
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, nullptr);
            VkQueueFamilyProperties queueFamilyProperties[queueFamilyPropertyCount];
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyPropertyCount, &queueFamilyProperties[0]);

            if(physicalDeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
            {
                vulkanDevices.push_back(
                    {
                        .physicalDevice = physicalDevice
                    }
                );    
                for(const auto & queueFamily : queueFamilyProperties)
                {
                    i32 queueFamilyIndex = 0;
                    VkBool32 surfaceSupported;

                    vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, queueFamilyIndex, surface, &surfaceSupported);
                    bool compute    =  queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT;
                    bool graphics   =  (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) && surfaceSupported;

                    VulkanDevice& currentVulkanDevice = vulkanDevices[physicalDeviceIndex];

                    if(graphics)        currentVulkanDevice.graphicsQueueFamilyIndices.push_back(queueFamilyIndex); 
                    if(compute)         currentVulkanDevice.computeQueueFamilyIndices.push_back(queueFamilyIndex);

                    if(!graphics && !compute) printf("createVulkanDevice : Not a compute or graphics Queue Family! %u", queueFamilyIndex);

                    currentVulkanDevice.queueFamilyProperties = queueFamily;
                    currentVulkanDevice.deviceQueueCreateInfos.push_back({
                        .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
                        .queueFamilyIndex = queueFamilyIndex++,
                        .queueCount = queueFamily.queueCount,
                        .pQueuePriorities = &queuePriorities[0]
                    });
                    queueFamilyIndex++;
                }
                physicalDeviceIndex++;
            }
        }

        // The VulkanDevice struct toReturn
        VulkanDevice toReturn = {};
        VkPhysicalDevice physicalDevice;
        bool foundDeviceThatCanSupport = false;
        for(const auto& vulkanDevice: vulkanDevices)
        {
            // TODO(Dustin) : This can be done better/more complete using a loop
            if(
                (glfwGetPhysicalDevicePresentationSupport(instance.instance, vulkanDevice.physicalDevice, vulkanDevice.deviceQueueCreateInfos[0].queueFamilyIndex) == GLFW_TRUE) &&
                vulkanDevice.graphicsQueueFamilyIndices.size() &&
                vulkanDevice.computeQueueFamilyIndices.size()
                )
            {
                toReturn = vulkanDevice;
                physicalDevice = vulkanDevice.physicalDevice;
                foundDeviceThatCanSupport = true;
                break;
            }
        }
        if(!foundDeviceThatCanSupport)
        {
            vulkanCrashOnError(VK_ERROR_INITIALIZATION_FAILED, "createVulkanDevice : No device supporting surface");
        }

        VkPhysicalDeviceFeatures physicalDeviceFeatures; // TODO (Dustin) : expect one of these and check if the physicaldevice can deliver or somethin?
        vkGetPhysicalDeviceFeatures(physicalDevice, &physicalDeviceFeatures);
        
        u32 availableDeviceExtensionPropertiesCount = 0;
        vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &availableDeviceExtensionPropertiesCount, nullptr);
        std::vector<VkExtensionProperties> availableDeviceExtensionProperties(availableDeviceExtensionPropertiesCount);
        vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &availableDeviceExtensionPropertiesCount, &availableDeviceExtensionProperties[0]);

        std::vector<char const *> deviceExtensions;
        printf("CreateVulkanDevice : Available Device Extensions\n");
        for(const auto& deviceExtensionProperty : availableDeviceExtensionProperties)
        {
            printf("Available Device Extension : %s (api version : %d)\n", deviceExtensionProperty.extensionName, deviceExtensionProperty.specVersion);
            if(strcmp(VK_KHR_SWAPCHAIN_EXTENSION_NAME, deviceExtensionProperty.extensionName) == 0)
            {
                deviceExtensions.push_back(deviceExtensionProperty.extensionName);
            }
        }

        // Queue Creation
        // Either we have one queue family that can do both compute and graphics
        // Or we have to separate queue families for this
        // Or we have a queue family that can do both but only supports one queue
        // Or we have a queue family that can do both and supports multiple queues

        if(toReturn.graphicsQueueFamilyIndices[0] == toReturn.computeQueueFamilyIndices[0])
        {
            if(toReturn.deviceQueueCreateInfos[0].queueCount > 1)
            {
                toReturn.usedDeviceQueueCreateInfos.push_back(toReturn.deviceQueueCreateInfos[0]);    
            } 
            #if 0
            else if (toReturn.deviceQueueCreateInfos.size() > toReturn.computeQueueFamilyIndices[1])
            {
                toReturn.usedDeviceQueueCreateInfos.push_back(toReturn.deviceQueueCreateInfos[0]);    
                toReturn.usedDeviceQueueCreateInfos.push_back(toReturn.deviceQueueCreateInfos[toReturn.computeQueueFamilyIndices[1]]);    
            } 
            #endif // TODO(Dustin) : This requires VK_SHARING_MODE_CONCURRENT
            else {
                // We can't make enough queues!
                vulkanCrashOnError(VK_ERROR_INITIALIZATION_FAILED, "createVulkanDevice : Device does not support enough queues!");
            }
        }
        VkDeviceCreateInfo deviceCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
            .queueCreateInfoCount = toReturn.usedDeviceQueueCreateInfos.size(),
            .pQueueCreateInfos = &toReturn.usedDeviceQueueCreateInfos[0],
            .pEnabledFeatures = &physicalDeviceFeatures,
            .ppEnabledExtensionNames = &deviceExtensions[0],
            .enabledExtensionCount = deviceExtensions.size()
        };

        vulkanCrashOnError(vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &toReturn.device), "createVulkanDevice : Creating Vulkan Logical Device!");

        printf("createVulkanDevice : Vulkan Logical Device created!\n");

        VmaAllocatorCreateInfo allocatorCreateInfo = 
        {
            .physicalDevice = toReturn.physicalDevice,
            .device = toReturn.device
        };
        
        vulkanCrashOnError(vmaCreateAllocator(&allocatorCreateInfo, &toReturn.allocator) , "createVulkanDevice : Creating VMA Allocator!");

        if(toReturn.usedDeviceQueueCreateInfos.size() > 1)
        {
            toReturn.graphicsQueueIndex = 0;
            toReturn.graphicsQueueFamilyIndex = toReturn.usedDeviceQueueCreateInfos[0].queueFamilyIndex;
            
            toReturn.computeQueueIndex = 0;
            toReturn.computeQueueFamilyIndex = toReturn.usedDeviceQueueCreateInfos[1].queueFamilyIndex;

        } else {
            toReturn.graphicsQueueIndex = 0;
            toReturn.graphicsQueueFamilyIndex = toReturn.usedDeviceQueueCreateInfos[0].queueFamilyIndex;
            
            toReturn.computeQueueIndex = 1;
            toReturn.computeQueueFamilyIndex = toReturn.usedDeviceQueueCreateInfos[0].queueFamilyIndex;
        }

        vkGetDeviceQueue(toReturn.device, toReturn.graphicsQueueFamilyIndex, toReturn.graphicsQueueIndex, &toReturn.graphicsQueue);
        vkGetDeviceQueue(toReturn.device, toReturn.computeQueueFamilyIndex, toReturn.computeQueueIndex, &toReturn.computeQueue);

        return toReturn;
    }

    VulkanSwapchain createVulkanSwapchain(
        GLFWwindow* window,
        VkSurfaceKHR surface,
        VulkanDevice device,
        u32 const swapchainImageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        u32 const swapchainImageViewAspectMask = VK_IMAGE_ASPECT_COLOR_BIT)
    {
        VkSurfaceCapabilitiesKHR surfaceCapabilities;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device.physicalDevice, surface, &surfaceCapabilities);

        int width  = 0;
        int height = 0;
        glfwGetWindowSize(window, &width, &height);
        VkExtent2D extent = 
        {
            .width = width,
            .height = height
        };

        if  (
            width > surfaceCapabilities.maxImageExtent.width    ||
            height > surfaceCapabilities.maxImageExtent.height  ||
            width < surfaceCapabilities.minImageExtent.width    ||
            height < surfaceCapabilities.minImageExtent.height  ) 
            {
                printf("createVulkanSwapchain : Window is bigger than the surface can handle?\n"); 
                exit(-1);
            }
        
        u32 presentModeCount = 0;
        vkGetPhysicalDeviceSurfacePresentModesKHR(device.physicalDevice, surface, &presentModeCount, nullptr);
        std::vector<VkPresentModeKHR> presentModes(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device.physicalDevice, surface, &presentModeCount, &presentModes[0]);

        VkPresentModeKHR chosenPresentMode;
        if      (presentModeAvailable(chosenPresentMode = VK_PRESENT_MODE_MAILBOX_KHR       , presentModes))  {IVAS_DEBUG(printf("Chosen present mode : VK_PRESENT_MODE_MAILBOX_KHR\n"););}
        else if (presentModeAvailable(chosenPresentMode = VK_PRESENT_MODE_FIFO_KHR          , presentModes))  {IVAS_DEBUG(printf("Chosen present mode : VK_PRESENT_MODE_FIFO_KHR\n"););}
        else if (presentModeAvailable(chosenPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR     , presentModes))  {IVAS_DEBUG(printf("Chosen present mode : VK_PRESENT_MODE_IMMEDIATE_KHR\n"););}
        else if (presentModeAvailable(chosenPresentMode = VK_PRESENT_MODE_FIFO_RELAXED_KHR  , presentModes))  {IVAS_DEBUG(printf("Chosen present mode : VK_PRESENT_MODE_FIFO_RELAXED_KHR\n"););}
        
        u32 surfaceFormatCount = 0;
        vkGetPhysicalDeviceSurfaceFormatsKHR(device.physicalDevice, surface, &surfaceFormatCount, nullptr); IVAS_DEBUG(printf("PhysicalDevice : Supported Surface Format count %d\n", surfaceFormatCount););
        std::vector<VkSurfaceFormatKHR> surfaceFormats(surfaceFormatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device.physicalDevice, surface, &surfaceFormatCount, &surfaceFormats[0]);

        VkBool32 surfaceSupported=false;
        vkGetPhysicalDeviceSurfaceSupportKHR(device.physicalDevice, device.graphicsQueueFamilyIndex, surface, &surfaceSupported);
        assert(surfaceSupported);

        /*
        bool formatSupported = false;
        VkColorSpaceKHR colorSpace;
        for(const auto & surfaceFormat : surfaceFormats)
        {
            IVAS_DEBUG(printf("Supported Format : %i", surfaceFormat.format););
            if( surfaceFormat.format == format )
            {
                formatSupported = true;
                colorSpace = surfaceFormat.colorSpace;
                printf("Format supported!");
            }
        }
        */
        VkColorSpaceKHR colorSpace = surfaceFormats[0].colorSpace;
        VkFormat format = surfaceFormats[0].format;

        // TODO : See if converted format is supported
        // TODO : Choose supported present mode

        u32 swapchainImageCount = imax(3, surfaceCapabilities.minImageCount);       
        VkSwapchainCreateInfoKHR swapchainCreateInfo = {
                .sType                  = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
                .surface                = surface,
                .minImageCount          = swapchainImageCount,
                .imageFormat            = format, // TODO : Create format checking function
                .imageColorSpace        = colorSpace,
                .imageExtent            = extent,
                .imageArrayLayers       = 1,
                .imageUsage             = swapchainImageUsage,
                .imageSharingMode       = VK_SHARING_MODE_EXCLUSIVE,
                .queueFamilyIndexCount  = 1,
                .pQueueFamilyIndices    = &device.usedDeviceQueueCreateInfos[0].queueFamilyIndex,
                .preTransform           = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
                .compositeAlpha         = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
                .presentMode            = chosenPresentMode,
                .clipped                = VK_FALSE
        };

        VkSwapchainKHR swapchain;
        vulkanCrashOnError(vkCreateSwapchainKHR(device.device, &swapchainCreateInfo, nullptr, &swapchain), "createVulkanSwapchain : Creating Swapchain");

        u32 actualSwapchainImageCount = 0;
        vkGetSwapchainImagesKHR(device.device, swapchain, &actualSwapchainImageCount, nullptr);
        std::vector<VkImage> swapchainImages(swapchainImageCount);
        vkGetSwapchainImagesKHR(device.device, swapchain, &actualSwapchainImageCount, &swapchainImages[0]);

        std::vector<VkImageView> swapchainImageViews(swapchainImageCount);
        std::vector<VkImageViewCreateInfo> swapchainImageViewCreateInfos(swapchainImageCount);
        u32 index=0;
        for(const auto & swapchainImage : swapchainImages)
        {
            swapchainImageViewCreateInfos[index] =  
            {
                .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                .image = swapchainImage,
                .viewType = VK_IMAGE_VIEW_TYPE_2D,
                .format = format,
                .components = {
                    .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .a = VK_COMPONENT_SWIZZLE_IDENTITY,
                },
                .subresourceRange = {
                    .aspectMask = swapchainImageViewAspectMask,
                    .baseMipLevel = 0,
                    .levelCount = VK_REMAINING_MIP_LEVELS,
                    .baseArrayLayer = 0,
                    .layerCount = VK_REMAINING_ARRAY_LAYERS
                }
            };
            vulkanCrashOnError(vkCreateImageView(device.device, &swapchainImageViewCreateInfos[index], nullptr, &swapchainImageViews[index]), "createVulkanSwapchain : Creating Swapchain Imageview");
            index++;
        }

        return {
            .swapchainCreateInfo = swapchainCreateInfo,
            .swapchain = swapchain,
            .surfaceFormat = surfaceFormats[0],
            .swapchainImages = swapchainImages,
            .swapchainImageViews = swapchainImageViews,
            .swapchainImageViewCreateInfos = swapchainImageViewCreateInfos
        };
    }

    VulkanCommandPool createVulkanCommandPool(VulkanDevice device)
    {
        VkCommandPoolCreateInfo commandPoolCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
            .queueFamilyIndex = device.usedDeviceQueueCreateInfos[0].queueFamilyIndex,
            .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT
        };

        VkCommandPool commandPool;
        vulkanCrashOnError(vkCreateCommandPool(device.device, &commandPoolCreateInfo, nullptr, &commandPool), "Creating CommandPool!");
        
        VkCommandBufferAllocateInfo primaryCommandBufferAllocateInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .commandPool = commandPool,
            .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            .commandBufferCount = 2
        };

        VkCommandBufferAllocateInfo copyCommandBufferAllocateInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .commandPool = commandPool,
            .level = VK_COMMAND_BUFFER_LEVEL_SECONDARY,
            .commandBufferCount = 2
        };

        VkCommandBufferAllocateInfo secondaryCommandBufferAllocateInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .commandPool = commandPool,
            .level = VK_COMMAND_BUFFER_LEVEL_SECONDARY,
            .commandBufferCount = 2
        };

        std::vector<VkCommandBuffer> primaryCommandBuffers(2);
        vulkanCrashOnError(vkAllocateCommandBuffers(device.device, &primaryCommandBufferAllocateInfo, &primaryCommandBuffers[0]), "Allocating Primary Command Buffers!");

        std::vector<VkCommandBuffer> copyCommandBuffers(2);
        vulkanCrashOnError(vkAllocateCommandBuffers(device.device, &copyCommandBufferAllocateInfo, &copyCommandBuffers[0]), "Allocating Copy Command Buffers!");

        std::vector<VkCommandBuffer> secondaryCommandBuffers(2);
        vulkanCrashOnError(vkAllocateCommandBuffers(device.device, &secondaryCommandBufferAllocateInfo, &secondaryCommandBuffers[0]), "Allocating Secondary Command Buffers!");

        VulkanCommandPool toReturn = {
            .commandPoolCreateInfo              = commandPoolCreateInfo,
            .commandPool                        = commandPool,
            .availablePrimaryCommandBuffer      = primaryCommandBuffers[0],
            .availableSecondaryCommandBuffer    = secondaryCommandBuffers[0],
            .availableCopyCommandBuffer         = copyCommandBuffers[0],
            .inUsePrimaryCommandBuffer          = primaryCommandBuffers[1],
            .inUseSecondaryCommandBuffer        = secondaryCommandBuffers[1],
            .inUseCopyCommandBuffer             = copyCommandBuffers[1]
        };
        return toReturn;
    }

    VkRenderPass createVkRenderPass(VulkanDevice device, VulkanSwapchain swapchain, VulkanDepthStencilImage depthStencilImage)
    {
        VkAttachmentDescription colorAttachmentDescription = {
            .format = swapchain.swapchainCreateInfo.imageFormat,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
        };

        VkAttachmentDescription depthAttachmentDescription = {
            .format = depthStencilImage.imageCreateInfo.format,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
        };

        VkAttachmentReference colorAttachmentReference = {
            .attachment = 0,
            .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        };

        VkAttachmentReference depthAttachmentReference = {
            .attachment = 1,
            .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL 
        };

        VkSubpassDescription subpassDescription =  {
            .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
            .colorAttachmentCount = 1,
            .pColorAttachments = &colorAttachmentReference,
            .pDepthStencilAttachment = &depthAttachmentReference
        };

        VkSubpassDependency dependency = {
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .srcAccessMask = 0,
            .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
        };

        std::vector<VkAttachmentDescription> attachments;
        attachments.push_back(colorAttachmentDescription);
        attachments.push_back(depthAttachmentDescription);

        VkRenderPassCreateInfo renderPassCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
            .attachmentCount = attachments.size(),
            .pAttachments = attachments.data(),
            .subpassCount = 1,
            .pSubpasses = &subpassDescription,
            .dependencyCount = 1,
            .pDependencies = &dependency
        };

        VkRenderPass renderPass;
        vulkanCrashOnError(vkCreateRenderPass(device.device, &renderPassCreateInfo, nullptr, &renderPass), "Creating RenderPass\n");

        return renderPass;
    }
    VulkanGraphicsPipeline createVulkanGraphicsPipeline(
        VulkanDevice device,
        VulkanSwapchain swapchain,
        VkRenderPass renderPass,
        VulkanShaderModule vertShader,
        VulkanShaderModule fragShader,
        VulkanVertexBuffer vertexBuffer,
        std::vector<VkPushConstantRange> pushConstantInfo,
        std::vector<VkDescriptorSetLayout> descriptorSetLayouts
        )
    {
        VkPipelineShaderStageCreateInfo shaderStages[] = {vertShader.pipelineShaderStageCreateInfo, fragShader.pipelineShaderStageCreateInfo};

        VkPipelineVertexInputStateCreateInfo vertexInputInfo = {
            .sType                              = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
            .vertexBindingDescriptionCount      = 1,
            .pVertexBindingDescriptions         = &vertexBuffer.inputBindingDescription,
            .vertexAttributeDescriptionCount    = vertexBuffer.attributeDescriptions.size(),
            .pVertexAttributeDescriptions       = &vertexBuffer.attributeDescriptions[0]
        };

        VkPipelineInputAssemblyStateCreateInfo inputAssembly = {
            .sType                  = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
            .topology               = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
            .primitiveRestartEnable = VK_FALSE
        };

        VkViewport viewport = {
            .x          = 0.0f,
            .y          = 0.0f,
            .width      = (float) swapchain.swapchainCreateInfo.imageExtent.width,
            .height     = (float) swapchain.swapchainCreateInfo.imageExtent.height,
            .minDepth   = 0.0f,
            .maxDepth   = 1.0f
        };

        VkRect2D scissor = {
            .offset = {
                0,
                0
                },
            .extent = swapchain.swapchainCreateInfo.imageExtent
        };

        VkPipelineViewportStateCreateInfo viewportState = {
            .sType          = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
            .viewportCount  = 1,
            .pViewports     = &viewport,
            .scissorCount   = 1,
            .pScissors      = &scissor
        };

        VkPipelineRasterizationStateCreateInfo rasterizer = {
            .sType                      = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
            .depthClampEnable           = VK_FALSE,
            .rasterizerDiscardEnable    = VK_FALSE,
            .polygonMode                = VK_POLYGON_MODE_FILL,
            .lineWidth                  = 1.0f,
            .cullMode                   = VK_CULL_MODE_BACK_BIT,
            .frontFace                  = VK_FRONT_FACE_COUNTER_CLOCKWISE,
            .depthBiasEnable            = VK_FALSE
        };

        VkPipelineMultisampleStateCreateInfo multisampling = {
            .sType                  = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
            .sampleShadingEnable    = VK_FALSE,
            .rasterizationSamples   = VK_SAMPLE_COUNT_1_BIT
        };

        VkPipelineColorBlendAttachmentState colorBlendAttachment = {
            .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
            .blendEnable    = VK_FALSE
        };

        VkPipelineDepthStencilStateCreateInfo depthStencil = {
            .sType              = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
            .depthTestEnable    = VK_TRUE,
            .depthWriteEnable   = VK_TRUE,
            .depthCompareOp     = VK_COMPARE_OP_LESS_OR_EQUAL,
            .minDepthBounds     = 0.0f,
            .maxDepthBounds     = 1.0f,
            .stencilTestEnable  = VK_FALSE,
            .front              = {},
            .back               = {}
        };

        VkPipelineColorBlendStateCreateInfo colorBlending = {
            .sType              = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
            .logicOpEnable      = VK_FALSE,
            .logicOp            = VK_LOGIC_OP_COPY,
            .attachmentCount    = 1,
            .pAttachments       = &colorBlendAttachment,
            .blendConstants[0]  = 0.0f,
            .blendConstants[1]  = 0.0f,
            .blendConstants[2]  = 0.0f,
            .blendConstants[3]  = 0.0f
        };

        VkPipelineLayoutCreateInfo pipelineLayoutInfo = {
            .sType                  = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
            .setLayoutCount         = descriptorSetLayouts.size(),
            .pSetLayouts            = &descriptorSetLayouts[0],
            .pushConstantRangeCount = pushConstantInfo.size(),
            .pPushConstantRanges    = pushConstantInfo.data()
        };

        VkPipelineLayout pipelineLayout;
        vulkanCrashOnError(vkCreatePipelineLayout(device.device, &pipelineLayoutInfo, nullptr, &pipelineLayout), "Creating pipeline layout!\n");

        VkGraphicsPipelineCreateInfo pipelineInfo{ 
            .sType                  = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
            .stageCount             = 2,
            .pStages                = shaderStages,
            .pVertexInputState      = &vertexInputInfo,
            .pInputAssemblyState    = &inputAssembly,
            .pViewportState         = &viewportState,
            .pRasterizationState    = &rasterizer,
            .pMultisampleState      = &multisampling,
            .pDepthStencilState     = &depthStencil,
            .pColorBlendState       = &colorBlending,
            .layout                 = pipelineLayout,
            .renderPass             = renderPass,
            .subpass                = 0,
            .basePipelineHandle     = VK_NULL_HANDLE
        };

        VkPipeline graphicsPipeline;
        vulkanCrashOnError (vkCreateGraphicsPipelines(device.device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline) , "Creating graphics pipeline!\n");

        //vkDestroyShaderModule(device, fragShaderModule, nullptr);
        //vkDestroyShaderModule(device, vertShaderModule, nullptr);

        VkShaderStageFlags shaderStageFlags = 0;
        for(const auto & shader : shaderStages)
        {
            shaderStageFlags |= shader.stage;
        }

        return {
            .graphicsPipelineCreateInfo = pipelineInfo,
            .pipeline                   = graphicsPipeline,
            .shaderStages               = shaderStageFlags
        };
    }

    VulkanShaderModule createVulkanShaderModule(VulkanDevice device, const char* shaderPath, VkShaderStageFlagBits shaderStage)
    {
        std::vector<u32> shaderCode = readFile32(shaderPath);
        assert(shaderCode.size() > 0);


        VkShaderModuleCreateInfo shaderModuleCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
            .codeSize = shaderCode.size() * sizeof(u32),
            .pCode = &shaderCode[0]
        };

        VkShaderModule shaderModule;
        vulkanCrashOnError(vkCreateShaderModule(device.device, &shaderModuleCreateInfo, nullptr, &shaderModule), "Creating ShaderModule" );

        VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = shaderStage,
            .module = shaderModule,
            .pName = "main"
        };

        return {
            .shaderModuleCreateInfo = shaderModuleCreateInfo,
            .shaderModule = shaderModule,
            .pipelineShaderStageCreateInfo = pipelineShaderStageCreateInfo
        };
    }

    VkFramebuffer createVulkanFramebuffer(
        VulkanDevice device,
        VkRenderPass renderPass,
        VkImageView swapchainImageView,
        VkImageView depthStencilImageView,
        u32 width,
        u32 height
        )
    {
        std::vector<VkImageView> imageViewAttachments;
        imageViewAttachments.push_back(swapchainImageView);
        imageViewAttachments.push_back(depthStencilImageView);

        VkFramebufferCreateInfo framebufferCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = renderPass,
            .attachmentCount = imageViewAttachments.size(),
            .pAttachments = &imageViewAttachments[0],
            .width = width,
            .height = height,
            .layers = 1
        };

        VkFramebuffer framebuffer;
        vkCreateFramebuffer(device.device, &framebufferCreateInfo, nullptr, &framebuffer);

        return framebuffer;
    }

    VkSemaphore createVkSemaphore(VulkanDevice device)
    {
        VkSemaphoreCreateInfo semaphoreCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
        };

        VkSemaphore semaphore;
        vkCreateSemaphore(device.device, &semaphoreCreateInfo, nullptr, &semaphore);

        return semaphore;
    }

    VkFence createVkFence(VulkanDevice device)
    {
        VkFenceCreateInfo fenceCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
        };

        VkFence fence;
        vkCreateFence(device.device, &fenceCreateInfo, nullptr, &fence);

        return fence;
    }

    VulkanDepthStencilImage createVulkanDepthStencilImage(
        VulkanDevice device,
        VkExtent3D extent
    )
    {
        VulkanDepthStencilImage toReturn = {
            .allocationCreateInfo = {
                .flags = 0,
                .usage = VMA_MEMORY_USAGE_GPU_ONLY, 
                .requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                .preferredFlags = 0,
                .memoryTypeBits = 0,
                .pool = VK_NULL_HANDLE,
                .pUserData = nullptr
            },
            .imageCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
                .imageType = VK_IMAGE_TYPE_2D,
                .format = findDepthStencilFormat(device),
                .extent = extent,
                .mipLevels = 1,
                .arrayLayers = 1,
                .samples = VK_SAMPLE_COUNT_1_BIT,
                .tiling = VK_IMAGE_TILING_OPTIMAL,
                .usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
                .queueFamilyIndexCount = 1,
                .pQueueFamilyIndices = &device.usedDeviceQueueCreateInfos[0].queueFamilyIndex,
                .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
            }
        };

        vulkanCrashOnError( 
            vmaCreateImage(
                device.allocator,
                &toReturn.imageCreateInfo,
                &toReturn.allocationCreateInfo,
                &toReturn.image,
                &toReturn.allocation,
                &toReturn.allocationInfo 
            ), 
            "createVulkanDepthStencilImage : Creating Image"
        );

        toReturn.depthImageView = {
            .imageViewCreateInfo = VkImageViewCreateInfo{
                .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                .image = toReturn.image,
                .viewType = VK_IMAGE_VIEW_TYPE_2D, // Convert imageType to ImageViewType,
                .format = toReturn.imageCreateInfo.format,
                .components = {
                    .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .a = VK_COMPONENT_SWIZZLE_IDENTITY
                },
                .subresourceRange = {
                    .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT, // Convert ImageUsageFlags to ImageAspectFlags
                    .baseMipLevel = 0,
                    .levelCount = VK_REMAINING_MIP_LEVELS,
                    .baseArrayLayer = 0,
                    .layerCount = VK_REMAINING_ARRAY_LAYERS
                }
            }
        };

        vulkanCrashOnError(
            vkCreateImageView(
                device.device,
                &toReturn.depthImageView.imageViewCreateInfo,
                nullptr,
                &toReturn.depthImageView.imageView
                ),
            "createVulkanDepthStencilImage : Creating Depth ImageView\n"
        );

        toReturn.stencilImageView = {
            .imageViewCreateInfo = VkImageViewCreateInfo{
                .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                .image = toReturn.image,
                .viewType = VK_IMAGE_VIEW_TYPE_2D, // Convert imageType to ImageViewType,
                .format = toReturn.imageCreateInfo.format,
                .components = {
                    .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .a = VK_COMPONENT_SWIZZLE_IDENTITY
                },
                .subresourceRange = {
                    .aspectMask = VK_IMAGE_ASPECT_STENCIL_BIT, // Convert ImageUsageFlags to ImageAspectFlags
                    .baseMipLevel = 0,
                    .levelCount = VK_REMAINING_MIP_LEVELS,
                    .baseArrayLayer = 0,
                    .layerCount = VK_REMAINING_ARRAY_LAYERS
                }
            }
        };

        vulkanCrashOnError(
            vkCreateImageView(
                device.device,
                &toReturn.stencilImageView.imageViewCreateInfo,
                nullptr,
                &toReturn.stencilImageView.imageView
                ),
            "createVulkanDepthStencilImage : Creating Stencil ImageView\n"
        );
        
        toReturn.depthStencilImageView = {
            .imageViewCreateInfo = VkImageViewCreateInfo{
                .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                .image = toReturn.image,
                .viewType = VK_IMAGE_VIEW_TYPE_2D, // Convert imageType to ImageViewType,
                .format = toReturn.imageCreateInfo.format,
                .components = {
                    .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                    .a = VK_COMPONENT_SWIZZLE_IDENTITY
                },
                .subresourceRange = {
                    .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT, // Convert ImageUsageFlags to ImageAspectFlags
                    .baseMipLevel = 0,
                    .levelCount = VK_REMAINING_MIP_LEVELS,
                    .baseArrayLayer = 0,
                    .layerCount = VK_REMAINING_ARRAY_LAYERS
                }
            }
        };

        vulkanCrashOnError(
            vkCreateImageView(
                device.device,
                &toReturn.depthStencilImageView.imageViewCreateInfo,
                nullptr,
                &toReturn.depthStencilImageView.imageView
                ),
            "createVulkanDepthStencilImage : Creating DepthStencil ImageView\n"
        );
        return toReturn;
    }


    VulkanVertexBuffer createVulkanVertexBuffer(
        VulkanDevice device,
        VulkanCommandPool commandPool,
        VkVertexInputBindingDescription inputBindingDescription,
        std::vector<VkVertexInputAttributeDescription> attributeDescriptions,
        u32 size,
        void* data
        )
    {
        VulkanVertexBuffer buffer = {
            .bufferCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                .size = size,
                .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
                .queueFamilyIndexCount = 1,
                .pQueueFamilyIndices = &device.usedDeviceQueueCreateInfos[0].queueFamilyIndex
            },
            .inputBindingDescription = inputBindingDescription,
            .allocationCreateInfo = {
                .usage = VMA_MEMORY_USAGE_GPU_ONLY
            },
            .attributeDescriptions = attributeDescriptions
        };

        vmaCreateBuffer(
            device.allocator, 
            &buffer.bufferCreateInfo, 
            &buffer.allocationCreateInfo, 
            &buffer.buffer, 
            &buffer.allocation, 
            &buffer.allocationInfo 
        );

        copyWithStagingBuffer(device, commandPool, buffer.buffer, size, data);
        return buffer;
        
    }


    u32 recordNextVulkanCommandBuffer(
        VulkanDevice device,
        VulkanCommandPool commandPool,
        VulkanSwapchain swapchain,
        VulkanGraphicsPipeline graphicsPipeline,
        VkSemaphore imageAvailable,
        VkRenderPass renderPass,
        u32 vertexCount,
        std::vector<VkFramebuffer> framebuffers,
        std::vector<VkBuffer> vertexBuffers,
        std::vector<VkDeviceSize> vertexBufferOffsets,
        u32 pushConstantSize,
        void* pushConstantData
    )
    {
        u32 nextImageIndex = 0;
        vkAcquireNextImageKHR(device.device, swapchain.swapchain, UINT64_MAX, imageAvailable, VK_NULL_HANDLE, &nextImageIndex);
        {
            std::array<VkClearValue, 2> clearValues = {};
            clearValues[0].color = {0.1f, 0.1f, 0.1f, 1.0f};
            clearValues[1].depthStencil = {1.0f, 0};

            VkCommandBufferBeginInfo beginInfo = {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
                //.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT   
            };

            vkBeginCommandBuffer(commandPool.availablePrimaryCommandBuffer, &beginInfo);

            vkCmdPushConstants(
                commandPool.availablePrimaryCommandBuffer,
                graphicsPipeline.graphicsPipelineCreateInfo.layout,
                graphicsPipeline.shaderStages,
                0,
                pushConstantSize,
                pushConstantData
            );

            VkRenderPassBeginInfo renderPassBeginInfo = {
                .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
                .renderPass = renderPass,
                .framebuffer = framebuffers[nextImageIndex],
                .renderArea = {
                    .offset = {
                        .x = 0,
                        .y = 0
                    },
                    .extent = swapchain.swapchainCreateInfo.imageExtent
                },
                .clearValueCount = clearValues.size(),
                .pClearValues = clearValues.data()

            };

            vkCmdBeginRenderPass(commandPool.availablePrimaryCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
            vkCmdBindPipeline(commandPool.availablePrimaryCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline.pipeline);
            //vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline.graphicsPipelineCreateInfo.layout, 0, 0, VK_NULL_HANDLE, 0, nullptr);
            vkCmdBindVertexBuffers(commandPool.availablePrimaryCommandBuffer, 0, vertexBuffers.size(), vertexBuffers.data(), vertexBufferOffsets.data());
            vkCmdDraw(commandPool.availablePrimaryCommandBuffer, vertexCount, 1, 0, 0);

            vkCmdEndRenderPass(commandPool.availablePrimaryCommandBuffer);
            vkEndCommandBuffer(commandPool.availablePrimaryCommandBuffer);
        }
        ivas::swap(commandPool.availablePrimaryCommandBuffer, commandPool.inUsePrimaryCommandBuffer);
        return nextImageIndex;
    }

    void draw(
        VulkanDevice device,
        VulkanCommandPool commandPool,
        VulkanSwapchain swapchain,
        VulkanGraphicsPipeline graphicsPipeline,
        VkRenderPass renderPass,
        VkSemaphore imageAvailable,
        VkSemaphore renderingFinished,
        VkFence completedQueueExecution,
        u32 vertexCount,
        std::vector<VkBuffer> vertexBuffers,
        std::vector<VkDeviceSize> vertexBufferOffsets,
        std::vector<VkFramebuffer> framebuffers,
        u32 pushConstantSize,
        void* pushConstantData
        )
    {
        // Record CommandBuffers
        u32 nextImageIndex = recordNextVulkanCommandBuffer(
            device,
            commandPool,
            swapchain,
            graphicsPipeline,
            imageAvailable,
            renderPass,
            vertexCount, // total vertices to be drawn
            framebuffers,
            vertexBuffers,
            vertexBufferOffsets,
            pushConstantSize,
            pushConstantData
        );

        VkPipelineStageFlags commandBufferWaitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
        VkSubmitInfo submitInfo = {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = &imageAvailable,
            .pWaitDstStageMask = commandBufferWaitStages,
            .signalSemaphoreCount = 1,
            .pSignalSemaphores = &renderingFinished,
            .commandBufferCount = 1,
            .pCommandBuffers = &commandPool.availablePrimaryCommandBuffer
        };
        vkQueueSubmit(device.graphicsQueue, 1, &submitInfo, completedQueueExecution);
        
        VkPresentInfoKHR presentInfo = {
            .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = &renderingFinished,
            .swapchainCount = 1,
            .pSwapchains = &swapchain.swapchain,
            .pImageIndices = &nextImageIndex
        };
        vkQueuePresentKHR(device.graphicsQueue, &presentInfo);

        vulkanCrashOnError(vkWaitForFences(device.device, 1, &completedQueueExecution, VK_TRUE, 100UL*1000UL*1000UL*1000UL), "Waiting for Queue Execution\n");

        VkFence fences[] = {completedQueueExecution};
        vkResetFences(device.device, 1, fences);

    }

    struct VulkanQueryPool{
        VkQueryPool queryPool;
        VkQueryPoolCreateInfo queryPoolCreateInfo;
        
        u32 queryResultBufferSize;
        VkDeviceSize queryResultBufferStride;
        void* queryResultBuffer;
    };
    VulkanQueryPool createVulkanQueryPool(
        VulkanDevice device,
        VkQueryType queryType,
        u32 queryCount,
        u32 pipelineStatisticFlags,
        u32 queryResultBufferSize,
        VkDeviceSize queryResultBufferStride,
        void* queryResultBuffer
    )
    {
        VulkanQueryPool queryPool = {
            .queryPoolCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO,
                .queryType = queryType,
                .queryCount = queryCount,
                .pipelineStatistics = pipelineStatisticFlags
            },
            .queryResultBufferSize = queryResultBufferSize,
            .queryResultBufferStride = queryResultBufferStride,
            .queryResultBuffer = queryResultBuffer, 
        };

        vkCreateQueryPool(device.device, &queryPool.queryPoolCreateInfo, nullptr, &queryPool.queryPool);
        return queryPool;
    }

    VulkanTestImplementation initVulkan(int width, int height){
        GLFWwindow* window          = createGLFWwindow(width, height);
        VulkanInstance instance     = createVulkanInstance();
        VkSurfaceKHR surface        = createVulkanSurface(window, instance.instance);
        VulkanDevice device         = createVulkanDevice(instance, surface);

        VulkanCommandPool commandPool   = createVulkanCommandPool(device);
        VkSemaphore imageAvailable      = createVkSemaphore(device);
        VkSemaphore renderingFinished   = createVkSemaphore(device);
        VkFence completedQueueExecution = createVkFence(device);

        VulkanSwapchain swapchain       = createVulkanSwapchain(window, surface, device);
        // Create DepthStencil
        VulkanDepthStencilImage depthStencil = createVulkanDepthStencilImage(
            device,
            VkExtent3D{
                .width = swapchain.swapchainCreateInfo.imageExtent.width,
                .height = swapchain.swapchainCreateInfo.imageExtent.height,
                .depth = 1
                }
        );

        VkRenderPass renderPass         = createVkRenderPass(device, swapchain, depthStencil);

        std::vector<VkFramebuffer> framebuffers;
        for(const auto& swapchainImageView : swapchain.swapchainImageViews)
        {
            framebuffers.push_back(
                createVulkanFramebuffer(
                    device,
                    renderPass,
                    swapchainImageView,
                    depthStencil.depthStencilImageView.imageView,
                    swapchain.swapchainCreateInfo.imageExtent.width,
                    swapchain.swapchainCreateInfo.imageExtent.height 
                )
            );
        }

        return {
            .window = window,
            .instance = instance,
            .surface = surface,
            .device = device,
            .swapchain = swapchain,
            .commandPool = commandPool,
            .renderPass = renderPass,
            .framebuffers = framebuffers,
            .imageAvailable = imageAvailable,
            .renderingFinished = renderingFinished,
            .completedQueueExecution = completedQueueExecution
        };
    }

    struct RTComputeShaderPushConstantData {
        vec3 bottomLeft;   f32 origin_x;
        vec3 bottomRight;  f32 origin_y;
        vec3 topLeft;      f32 origin_z;
        vec3 topRight;     u32 frame;
    };

    struct RTComputeShaderInputDescription{
        VkDescriptorSetLayout   descriptorSetLayout;
        VkDescriptorSet         descriptorSet;
        VkDescriptorPool        descriptorPool;
    
        VkDescriptorSetAllocateInfo descriptorSetAllocateInfo;

        VkPushConstantRange pushConstantRange = {
            .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
            .size =  sizeof(RTComputeShaderPushConstantData)
        };

        VkDescriptorSetLayoutBinding bindings[6] = {
    	{ .binding = 0, .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, .descriptorCount = 1, .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT, .pImmutableSamplers = nullptr },
    	{ .binding = 1, .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, .descriptorCount = 1, .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT, .pImmutableSamplers = nullptr },
    	{ .binding = 2, .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, .descriptorCount = 1, .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT, .pImmutableSamplers = nullptr },
    	{ .binding = 3, .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, .descriptorCount = 1, .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT, .pImmutableSamplers = nullptr },
    	{ .binding = 4, .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, .descriptorCount = 1, .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT, .pImmutableSamplers = nullptr },
    	{ .binding = 5, .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, .descriptorCount = 1, .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT, .pImmutableSamplers = nullptr }
        };

        VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            .bindingCount = SIZE(bindings),
            .pBindings = &bindings[0]
        };

        VkDescriptorPoolSize descriptorPoolSizes[2] = {
            {.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, .descriptorCount = 3},
            {.type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, .descriptorCount = 3}
        };

        VkDescriptorPoolCreateInfo descriptorPoolCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO, .pNext = nullptr, .flags = 0, .maxSets = 1, .poolSizeCount = SIZE(descriptorPoolSizes), .pPoolSizes = &descriptorPoolSizes[0]
        };

        VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo;
        VkPipelineLayout pipelineLayout;
    };
    RTComputeShaderInputDescription createRTComputeShaderInputDescription(VulkanDevice device) 
    {   
        RTComputeShaderInputDescription toReturn = {}; 
        vulkanCrashOnError(
            vkCreateDescriptorSetLayout(device.device, &toReturn.descriptorSetLayoutCreateInfo, nullptr, &toReturn.descriptorSetLayout),
            "createRTComputeShaderInputDescription : Failed to create descriptor set layout\n"
        );

        vulkanCrashOnError(
            vkCreateDescriptorPool(device.device, &toReturn.descriptorPoolCreateInfo, nullptr, &toReturn.descriptorPool),
            "createRTComputeShaderInputDescription : Failed to create descriptor pool\n"
        );

        // allocate descriptor set
        toReturn.descriptorSetAllocateInfo = {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
            .descriptorPool = toReturn.descriptorPool,
            .descriptorSetCount = 1,
            .pSetLayouts = &toReturn.descriptorSetLayout,
        };

        vulkanCrashOnError(
            vkAllocateDescriptorSets(device.device, &toReturn.descriptorSetAllocateInfo, &toReturn.descriptorSet),
            "createRTComputeShaderInputDescription : Failed to allocate buffer descriptor set\n"
        );

        toReturn.pipelineLayoutCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
            .setLayoutCount = 1,
            .pSetLayouts = &toReturn.descriptorSetLayout,
            .pushConstantRangeCount = 1,
            .pPushConstantRanges = &toReturn.pushConstantRange
        };
        vulkanCrashOnError(
            vkCreatePipelineLayout(device.device, &toReturn.pipelineLayoutCreateInfo, nullptr, &toReturn.pipelineLayout), 
            "createRTComputePipeline : Failed to create pipelineLayout\n"
        );
        return toReturn;
    }

    struct RTComputePipeline{
        VkPipeline       pipeline;
        VkComputePipelineCreateInfo pipelineCreateInfo;
    };

    RTComputePipeline createRTComputePipeline(
        VulkanDevice                    device, 
        VulkanShaderModule              computeShader,
        RTComputeShaderInputDescription inputDescription
    )
    {
        RTComputePipeline toReturn = {};

        toReturn.pipelineCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
            .stage = computeShader.pipelineShaderStageCreateInfo,
            .layout = inputDescription.pipelineLayout,
        };

        vulkanCrashOnError(vkCreateComputePipelines(device.device, VK_NULL_HANDLE, 1, &toReturn.pipelineCreateInfo, nullptr, &toReturn.pipeline),
            "createRTComputePipeline : Failed to create compute pipeline\n"
        );

        return toReturn;
    }

    struct RTStorageImage{
        VkImage image;
        VkImageCreateInfo imageCreateInfo;

        VkImageView imageView;
        VkImageViewCreateInfo imageViewCreateInfo;

        VkImageMemoryBarrier imgMemBarrier;

        VmaAllocationCreateInfo allocationCreateInfo;
        VmaAllocation allocation;
        VmaAllocationInfo allocationInfo;
    };
    RTStorageImage createRTStorageImage(
        VulkanDevice device,
        VkExtent3D imageExtent
    )
    {
        RTStorageImage toReturn = {
            .imageCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
                .imageType = VK_IMAGE_TYPE_2D,
                .format = VK_FORMAT_R32G32B32A32_SFLOAT,
                .extent = imageExtent,
                .mipLevels = 1,
                .arrayLayers = 1,
                .samples = VK_SAMPLE_COUNT_1_BIT,
                .tiling = VK_IMAGE_TILING_OPTIMAL,
                .usage = VK_IMAGE_USAGE_STORAGE_BIT,
                .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
                .queueFamilyIndexCount = 1,
                .pQueueFamilyIndices = &device.computeQueueFamilyIndex,
                .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
            },
            .allocationCreateInfo = {
                .flags = 0,
                .usage = VMA_MEMORY_USAGE_GPU_ONLY,
                .requiredFlags = 0,
                .preferredFlags = 0,
                .memoryTypeBits = 0,
                .pool = VK_NULL_HANDLE,
                .pUserData = nullptr    
            }
        };

        vulkanCrashOnError(
            vmaCreateImage(device.allocator, &toReturn.imageCreateInfo, &toReturn.allocationCreateInfo, &toReturn.image, &toReturn.allocation, &toReturn.allocationInfo),
            "createRTStorageImage : Failed to create image"
        );

        toReturn.imageViewCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .image = toReturn.image,
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = VK_FORMAT_R32G32B32A32_SFLOAT,
            .components = {
                .r = VK_COMPONENT_SWIZZLE_IDENTITY,
                .g = VK_COMPONENT_SWIZZLE_IDENTITY,
                .b = VK_COMPONENT_SWIZZLE_IDENTITY,
                .a = VK_COMPONENT_SWIZZLE_IDENTITY 
            },
            .subresourceRange = {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount =- VK_REMAINING_MIP_LEVELS,
                .baseArrayLayer = 0,
                .layerCount = VK_REMAINING_ARRAY_LAYERS
            }
        };  

        vulkanCrashOnError(
            vkCreateImageView(device.device, &toReturn.imageViewCreateInfo, nullptr, &toReturn.imageView),
            "createRTStorageImage : Failed to create imageView"
        );

        toReturn.imgMemBarrier = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .srcAccessMask = 0,
            .dstAccessMask = 0,
            .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .newLayout = VK_IMAGE_LAYOUT_GENERAL,
            .srcQueueFamilyIndex = device.graphicsQueueFamilyIndex,
            .dstQueueFamilyIndex = device.graphicsQueueFamilyIndex,
            .image = toReturn.image,
            .subresourceRange = toReturn.imageViewCreateInfo.subresourceRange
        };
        return toReturn;
    }


    struct RTBuffer{
        VkBuffer buffer;
        VkBufferCreateInfo bufferCreateInfo;
        VmaAllocationCreateInfo allocationCreateInfo;
        VmaAllocation allocation;
        VmaAllocationInfo allocationInfo;
    };
    RTBuffer createRTBuffer(
        VulkanDevice device,
        u32 const bufferSize,
        int const usageFlags
    )
    {   
        RTBuffer rtBuffer = {
            .bufferCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                .size = bufferSize,
                .usage = usageFlags,
                .sharingMode = VK_SHARING_MODE_EXCLUSIVE
            },
            .allocationCreateInfo ={
                .flags = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT,
                .usage = VMA_MEMORY_USAGE_GPU_ONLY,
                .requiredFlags = 0,
                .preferredFlags = 0,
                .memoryTypeBits = 0,
                .pool = VK_NULL_HANDLE,
                .pUserData = nullptr
            }
        };

        vulkanCrashOnError(
            vmaCreateBuffer(
                device.allocator,
                &rtBuffer.bufferCreateInfo,
                &rtBuffer.allocationCreateInfo,
                &rtBuffer.buffer,
                &rtBuffer.allocation,
                &rtBuffer.allocationInfo
            ),
            "createRTBuffer : Could not create RTBuffer \n"
        );
        
        return rtBuffer;
    }

    void updateRTDescriptors(
        VulkanDevice device,
        RTComputeShaderInputDescription inputDescription,
        VkDescriptorImageInfo currentRead,
        VkDescriptorImageInfo currentWrite,
        VkDescriptorImageInfo nextOutput,
        VkDescriptorBufferInfo triangleBuffer,
        VkDescriptorBufferInfo lightIndexBuffer,
        VkDescriptorBufferInfo bvhBuffer)
    {
        VkWriteDescriptorSet writeDescriptors[] = {
            {   
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = inputDescription.descriptorSet,
                .dstBinding = 0,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                .pImageInfo = &currentRead,
            }, {   
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = inputDescription.descriptorSet,
                .dstBinding = 1,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                .pImageInfo = &currentWrite,
            }, {   
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = inputDescription.descriptorSet,
                .dstBinding = 2,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                .pImageInfo = &nextOutput
            }, {   
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = inputDescription.descriptorSet,
                .dstBinding = 3,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .pBufferInfo = &triangleBuffer
            }, {   
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = inputDescription.descriptorSet,
                .dstBinding = 4,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .pBufferInfo = &lightIndexBuffer
            }, {   
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = inputDescription.descriptorSet,
                .dstBinding = 5,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .pBufferInfo = &bvhBuffer
            }
        };

        vkUpdateDescriptorSets(device.device, SIZE(writeDescriptors), &writeDescriptors[0], 0, nullptr);
    }

    struct RTRenderPass{
        VkRenderPass renderPass;
        VkRenderPassCreateInfo renderPassCreateInfo;
    };
    RTRenderPass createRTRenderPass(
        VulkanDevice device,
        VulkanSwapchain swapchain,
        RTStorageImage bufferImage1,
        RTStorageImage bufferImage2
    )
    {
        VkAttachmentReference colorOutputReference = {
            .attachment = 0,
            .layout = VK_IMAGE_LAYOUT_GENERAL
        };
        VkAttachmentReference bufferImage1Reference = {
            .attachment = 1,
            .layout = VK_IMAGE_LAYOUT_GENERAL
        };
        VkAttachmentReference bufferImage2Reference = {
            .attachment = 2,
            .layout = VK_IMAGE_LAYOUT_GENERAL
        };

        VkAttachmentDescription colorOutputDescription = {
            .format = swapchain.swapchainCreateInfo.imageFormat,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
        };

        VkAttachmentDescription bufferImage1Description = {
            .format = bufferImage1.imageCreateInfo.format,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_LOAD,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_GENERAL,
            .finalLayout = VK_IMAGE_LAYOUT_GENERAL
        };

        VkAttachmentDescription bufferImage2Description = {
            .format = bufferImage2.imageCreateInfo.format,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_LOAD,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_GENERAL,
            .finalLayout = VK_IMAGE_LAYOUT_GENERAL
        };

        VkAttachmentDescription attachmentDescriptions[] = {
            colorOutputDescription,
            bufferImage1Description,
            bufferImage2Description
        };

        u32 attachmentIndices[] = {
            colorOutputReference.attachment,
            bufferImage1Reference.attachment,
            bufferImage2Reference.attachment
        };

        // Subpass - Allow all the images to be used, store the buffer images
        VkSubpassDescription subpass_RT =  {
            .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
            .inputAttachmentCount = 0,
            .pInputAttachments = VK_NULL_HANDLE,
            .colorAttachmentCount = 0,
            .pColorAttachments = VK_NULL_HANDLE,
            .pResolveAttachments = VK_NULL_HANDLE,
            .pDepthStencilAttachment = VK_NULL_HANDLE,
            .preserveAttachmentCount = SIZE(attachmentIndices),
            .pPreserveAttachments = &attachmentIndices[0]
        };

        VkSubpassDependency dependency_RT = {
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            .srcAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT,
            .dstAccessMask = 0,
            .dependencyFlags = 0
        };
        // Subpass - Allow all the images to be used, store the buffer images

        VkRenderPassCreateInfo renderPassCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
            .attachmentCount = SIZE(attachmentDescriptions),
            .pAttachments = &attachmentDescriptions[0],
            .subpassCount = 1,
            .pSubpasses = &subpass_RT,
            .dependencyCount = 1,
            .pDependencies = &dependency_RT
        };

        RTRenderPass toReturn = {
            .renderPassCreateInfo = renderPassCreateInfo
        };
        vulkanCrashOnError(
            vkCreateRenderPass(device.device, &toReturn.renderPassCreateInfo, nullptr, &toReturn.renderPass),
            "createRTRenderPass : Failed to create RenderPass \n"
        );
        return toReturn;
    }

    VkFramebuffer createRTFramebuffer(
        const VulkanDevice& device,
        const RTRenderPass& renderPass,
        const VkImageView& swapchainImageView,
        const VkImageView& imageView_bufferImage1,
        const VkImageView& imageView_bufferImage2,
        u32 const width,
        u32 const height
    )
    {
        std::vector<VkImageView> imageViewAttachments;
        imageViewAttachments.push_back(swapchainImageView);
        imageViewAttachments.push_back(imageView_bufferImage1);
        imageViewAttachments.push_back(imageView_bufferImage2);

        VkFramebufferCreateInfo framebufferCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = renderPass.renderPass,
            .attachmentCount = imageViewAttachments.size(),
            .pAttachments = &imageViewAttachments[0],
            .width = width,
            .height = height,
            .layers = 1
        };

        VkFramebuffer framebuffer;
        vkCreateFramebuffer(device.device, &framebufferCreateInfo, nullptr, &framebuffer);

        return framebuffer;

    }

    void initRTStorageImgs(
        VulkanDevice device,
        VulkanCommandPool commandPool,
        RTStorageImage read,
        RTStorageImage write
    )
    {
        VkCommandBufferBeginInfo beginInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
        };
        vulkanCrashOnError(
            vkBeginCommandBuffer(commandPool.availablePrimaryCommandBuffer, &beginInfo), 
            "initRTStorageImgs : Failed to begin command buffer \n"
        );

        VkImageMemoryBarrier memBarriers[2] = {read.imgMemBarrier, write.imgMemBarrier};

        vkCmdPipelineBarrier(
            commandPool.availablePrimaryCommandBuffer,
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
            0,
            0,
            VK_NULL_HANDLE,
            0,
            VK_NULL_HANDLE,
            2,
            &memBarriers[0]
        );

        vulkanCrashOnError(
            vkEndCommandBuffer(commandPool.availablePrimaryCommandBuffer), 
            "initRTStorageImgs : Failed to end command buffer \n"
        );
        
        VkSubmitInfo submitInfo = {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .commandBufferCount = 1,
            .pCommandBuffers = &commandPool.availablePrimaryCommandBuffer 
        };

        VkFence completedExecution = createVkFence(device);

        vulkanCrashOnError(
            vkQueueSubmit(device.graphicsQueue, 1, &submitInfo, completedExecution),
            "initRTStorageImgs : Failed to submit to queue \n"
        );
        vulkanCrashOnError(
            vkWaitForFences(device.device, 1, &completedExecution, VK_TRUE, u64max),
            "initRTStorageImgs : Fence timeout \n"
        );

        vkDestroyFence(device.device, completedExecution, nullptr);
    }

    struct RTTexelBuffer{
        VkBuffer buffer;
        VkBufferView bufferView;

        VkBufferCreateInfo bufferCreateInfo;
        VkBufferViewCreateInfo bufferViewCreateInfo;

        VmaAllocationCreateInfo allocationCreateInfo;
        VmaAllocation allocation;
        VmaAllocationInfo allocationInfo;
    };
    RTTexelBuffer createRTTexelBuffer(
        VulkanDevice device,
        u32 const reqBufferSize
    )
    {   
        RTTexelBuffer rtTexelBuffer = {
            .bufferCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                .size = reqBufferSize,
                .usage = VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT,
                .sharingMode = VK_SHARING_MODE_EXCLUSIVE
            },
            .allocationCreateInfo ={
                .flags = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT,
                .usage = VMA_MEMORY_USAGE_GPU_ONLY,
                .requiredFlags = 0,
                .preferredFlags = 0,
                .memoryTypeBits = 0,
                .pool = VK_NULL_HANDLE,
                .pUserData = nullptr
            }
        };

        vulkanCrashOnError(
            vmaCreateBuffer(
                device.allocator,
                &rtTexelBuffer.bufferCreateInfo,
                &rtTexelBuffer.allocationCreateInfo,
                &rtTexelBuffer.buffer,
                &rtTexelBuffer.allocation,
                &rtTexelBuffer.allocationInfo
            ),
            "createRTTexelBuffer : Could not create RTTexelBuffer \n"
        );

        VkFormatFeatureFlags formatFeatureFlags = 
            VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_BIT;

        if(
            !isFormatSupportedWithFeatures(
                device,
                VK_FORMAT_R32G32B32A32_SFLOAT,
                0,
                0,
                formatFeatureFlags
            )
        ){
            printf("createRTTexelBuffer : format not supported \n");
            assert(false);
        };

        rtTexelBuffer.bufferViewCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO,
            .buffer = rtTexelBuffer.buffer,
            .format = VK_FORMAT_R32G32B32A32_SFLOAT,
            .offset = rtTexelBuffer.allocationInfo.offset,
            .range = rtTexelBuffer.allocationInfo.size
        };

        vulkanCrashOnError(
            vkCreateBufferView(device.device, &rtTexelBuffer.bufferViewCreateInfo, nullptr, &rtTexelBuffer.bufferView),
            "createRTTexelBuffer : Could not create BufferView \n"
        );
        
        return rtTexelBuffer;
    }
    struct RTTest{
        GLFWwindow* window;
        VulkanInstance instance;
        VkSurfaceKHR surface;
        VulkanDevice device;
        VulkanCommandPool commandPool;
        VulkanSwapchain swapchain;

        RTStorageImage currentRead;
        RTStorageImage currentWrite;

        RTRenderPass renderPass;
        std::vector<VkFramebuffer> framebuffers;

        RTComputeShaderInputDescription inputDescription;

        VulkanShaderModule computeShader;
        RTComputePipeline computePipeline;
    };
    RTTest initRT(int width, int height, char const * spvPath)
    {
        GLFWwindow* window          = createGLFWwindow(width,height);
        VulkanInstance instance     = createVulkanInstance();
        VkSurfaceKHR surface        = createVulkanSurface(window, instance.instance);
        VulkanDevice device         = createVulkanDevice(instance, surface);
        VulkanCommandPool commandPool   = createVulkanCommandPool(device);

        VulkanSwapchain swapchain       = createVulkanSwapchain(window, surface, device, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT, VK_IMAGE_ASPECT_COLOR_BIT);

        VkExtent3D swapchainExtent      = {.width = swapchain.swapchainCreateInfo.imageExtent.width, .height = swapchain.swapchainCreateInfo.imageExtent.height, .depth = 1};

        RTStorageImage currentRead = createRTStorageImage(device, swapchainExtent);
        RTStorageImage currentWrite = createRTStorageImage(device, swapchainExtent);

        initRTStorageImgs(device, commandPool, currentRead, currentWrite);

        RTRenderPass renderPass     = createRTRenderPass(device, swapchain, currentRead, currentWrite);

        std::vector<VkFramebuffer> framebuffers;
        for(const auto& swapchainImageView : swapchain.swapchainImageViews)
        {
            framebuffers.push_back(
                createRTFramebuffer(
                    device,
                    renderPass,
                    swapchainImageView,
                    currentRead.imageView,
                    currentWrite.imageView,
                    swapchain.swapchainCreateInfo.imageExtent.width,
                    swapchain.swapchainCreateInfo.imageExtent.height 
                )
            );
        }

        RTComputeShaderInputDescription inputDescription = createRTComputeShaderInputDescription(device) ;

        VulkanShaderModule computeShader = createVulkanShaderModule(device, spvPath, VK_SHADER_STAGE_COMPUTE_BIT);
        RTComputePipeline computePipeline = createRTComputePipeline(device, computeShader, inputDescription);

        return {
            .window = window,         
            .instance = instance,
            .surface = surface,
            .device = device,
            .commandPool = commandPool,     
            .swapchain = swapchain,
            .currentRead = currentRead,
            .currentWrite = currentWrite,
            .renderPass = renderPass,
            .framebuffers = framebuffers,
            .inputDescription = inputDescription,
            .computeShader = computeShader,
            .computePipeline = computePipeline
        };
    }

    void raytrace(
        VulkanDevice device,
        VulkanCommandPool commandPool,
        VulkanSwapchain swapchain,

        RTStorageImage &currentRead,
        RTStorageImage &currentWrite,
        RTBuffer triangleBuffer,
        RTBuffer lightIndexBuffer,
        RTBuffer bvhBuffer,
        RTComputeShaderInputDescription computeShaderInputDescription,
        RTComputeShaderPushConstantData pushConstantData,

        RTComputePipeline computePipeline,

        VkSemaphore imageAvailable,
        VkSemaphore renderingFinished,
        VkFence completedQueueExecution,

        VulkanQueryPool timestampQueryPool
    )
    {
        // We don't need to wait for the acquireNextImage, we just need to check that the return code is positive.
        u32 nextImageIndex = 0;
        while(vkAcquireNextImageKHR(device.device, swapchain.swapchain, u64max, imageAvailable, VK_NULL_HANDLE, &nextImageIndex) <  0);

        VkDescriptorImageInfo outputImageInfo =
        {
            .imageView = swapchain.swapchainImageViews[nextImageIndex],
            .imageLayout = VK_IMAGE_LAYOUT_GENERAL
        };
        VkDescriptorImageInfo readImageInfo =
        {
            .imageView = currentRead.imageView,
            .imageLayout = VK_IMAGE_LAYOUT_GENERAL
        };
        VkDescriptorImageInfo writeImageInfo =
        {
            .imageView = currentWrite.imageView,
            .imageLayout = VK_IMAGE_LAYOUT_GENERAL
        };
        VkDescriptorBufferInfo triangleBufferInfo = {
            .buffer = triangleBuffer.buffer,
            .offset = triangleBuffer.allocationInfo.offset,
            .range  = VK_WHOLE_SIZE
        };
        VkDescriptorBufferInfo lightIndexBufferInfo = {
            .buffer = lightIndexBuffer.buffer,
            .offset = lightIndexBuffer.allocationInfo.offset,
            .range  = VK_WHOLE_SIZE
        };
        VkDescriptorBufferInfo bvhBufferInfo = {
            .buffer = bvhBuffer.buffer,
            .offset = bvhBuffer.allocationInfo.offset,
            .range  = VK_WHOLE_SIZE
        };
        // Update Descriptors
        updateRTDescriptors(
            device,
            computeShaderInputDescription,
            readImageInfo,
            writeImageInfo,
            outputImageInfo,
            triangleBufferInfo,
            lightIndexBufferInfo,
            bvhBufferInfo);
        ivas::swap(currentRead, currentWrite);

        // Create Memory barriers
        VkImageMemoryBarrier memBarrier_undefinedToWrite = 
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
            .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .newLayout = VK_IMAGE_LAYOUT_GENERAL,
            .srcQueueFamilyIndex = device.graphicsQueueFamilyIndex,
            .dstQueueFamilyIndex = device.computeQueueFamilyIndex,
            .image = swapchain.swapchainImages[nextImageIndex],
            .subresourceRange = swapchain.swapchainImageViewCreateInfos[nextImageIndex].subresourceRange
        };

        VkImageMemoryBarrier memBarrier_writeToPresent = 
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
            .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
            .oldLayout = VK_IMAGE_LAYOUT_GENERAL,
            .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            .srcQueueFamilyIndex = device.computeQueueFamilyIndex,
            .dstQueueFamilyIndex = device.graphicsQueueFamilyIndex,
            .image = swapchain.swapchainImages[nextImageIndex],
            .subresourceRange = swapchain.swapchainImageViewCreateInfos[nextImageIndex].subresourceRange
        };

        // Record Command Buffers
        VkCommandBufferBeginInfo beginInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
        };           
        
        vkBeginCommandBuffer(commandPool.availablePrimaryCommandBuffer, &beginInfo);

        vkCmdResetQueryPool(commandPool.availablePrimaryCommandBuffer, timestampQueryPool.queryPool, 0, timestampQueryPool.queryPoolCreateInfo.queryCount-1);
        vkCmdWriteTimestamp(commandPool.availablePrimaryCommandBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, timestampQueryPool.queryPool, 0);

        vkCmdPushConstants(commandPool.availablePrimaryCommandBuffer, 
                computePipeline.pipelineCreateInfo.layout, 
                VK_SHADER_STAGE_COMPUTE_BIT, 0, 
                sizeof(pushConstantData), &pushConstantData);
        vkCmdBindPipeline(commandPool.availablePrimaryCommandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, computePipeline.pipeline);
        vkCmdBindDescriptorSets(commandPool.availablePrimaryCommandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, computePipeline.pipelineCreateInfo.layout, 0, 1, &computeShaderInputDescription.descriptorSet, 0, nullptr);
        vkCmdPipelineBarrier(
            commandPool.availablePrimaryCommandBuffer,
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
            0,
            0,
            0,
            0,
            VK_NULL_HANDLE,
            1,
            &memBarrier_undefinedToWrite
            );
        vkCmdDispatch(
            commandPool.availablePrimaryCommandBuffer,
            (u32)ceil(swapchain.swapchainCreateInfo.imageExtent.width / 32.f),
            (u32)ceil(swapchain.swapchainCreateInfo.imageExtent.height/ 32.f),
            1
            );
        vkCmdPipelineBarrier(
            commandPool.availablePrimaryCommandBuffer,
            VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
            VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            0,
            0,
            0,
            0,
            VK_NULL_HANDLE,
            1,
            &memBarrier_writeToPresent
            );

        vkCmdWriteTimestamp(commandPool.availablePrimaryCommandBuffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, timestampQueryPool.queryPool, 1);
        vkEndCommandBuffer(commandPool.availablePrimaryCommandBuffer);

        // Submit to Queue
        VkPipelineStageFlags commandBufferWaitStages[1] = {VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT};
        VkSemaphore waitSemaphores[1] = {imageAvailable};

        VkSubmitInfo submitInfo = {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = waitSemaphores,
            .pWaitDstStageMask = commandBufferWaitStages,
            .signalSemaphoreCount = 1,
            .pSignalSemaphores = &renderingFinished,
            .commandBufferCount = 1,
            .pCommandBuffers = &commandPool.availablePrimaryCommandBuffer
        };
        vkQueueSubmit(device.computeQueue, 1, &submitInfo, completedQueueExecution);
        ivas::swap(commandPool.availablePrimaryCommandBuffer, commandPool.inUsePrimaryCommandBuffer);
        while(vkWaitForFences(device.device, 1, &completedQueueExecution, VK_TRUE, u64max) < 0 );

        vulkanCrashOnError(
            vkGetQueryPoolResults(
                device.device,
                timestampQueryPool.queryPool,
                0,
                timestampQueryPool.queryPoolCreateInfo.queryCount,
                timestampQueryPool.queryResultBufferSize,
                timestampQueryPool.queryResultBuffer,
                timestampQueryPool.queryResultBufferStride,
                VK_QUERY_RESULT_64_BIT | VK_QUERY_RESULT_WAIT_BIT
                ),
                "raytrace : retrieving timestamp query results failed!");

        vkResetFences(device.device, 1, &completedQueueExecution);
        
        // Present
        VkPresentInfoKHR presentInfo = {
            .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = &renderingFinished,
            .swapchainCount = 1,
            .pSwapchains = &swapchain.swapchain,
            .pImageIndices = &nextImageIndex
        };
        vkQueuePresentKHR(device.graphicsQueue, &presentInfo);
    }
}


//  /$$    /$$           /$$ /$$                                 /$$   /$$                                                                                    
// | $$   | $$          | $$| $$                                | $$$ | $$                                                                                    
// | $$   | $$ /$$   /$$| $$| $$   /$$  /$$$$$$  /$$$$$$$       | $$$$| $$  /$$$$$$  /$$$$$$/$$$$   /$$$$$$   /$$$$$$$  /$$$$$$   /$$$$$$   /$$$$$$$  /$$$$$$ 
// |  $$ / $$/| $$  | $$| $$| $$  /$$/ |____  $$| $$__  $$      | $$ $$ $$ |____  $$| $$_  $$_  $$ /$$__  $$ /$$_____/ /$$__  $$ |____  $$ /$$_____/ /$$__  $$
//  \  $$ $$/ | $$  | $$| $$| $$$$$$/   /$$$$$$$| $$  \ $$      | $$  $$$$  /$$$$$$$| $$ \ $$ \ $$| $$$$$$$$|  $$$$$$ | $$  \ $$  /$$$$$$$| $$      | $$$$$$$$
//   \  $$$/  | $$  | $$| $$| $$_  $$  /$$__  $$| $$  | $$      | $$\  $$$ /$$__  $$| $$ | $$ | $$| $$_____/ \____  $$| $$  | $$ /$$__  $$| $$      | $$_____/
//    \  $/   |  $$$$$$/| $$| $$ \  $$|  $$$$$$$| $$  | $$      | $$ \  $$|  $$$$$$$| $$ | $$ | $$|  $$$$$$$ /$$$$$$$/| $$$$$$$/|  $$$$$$$|  $$$$$$$|  $$$$$$$
//     \_/     \______/ |__/|__/  \__/ \_______/|__/  |__/      |__/  \__/ \_______/|__/ |__/ |__/ \_______/|_______/ | $$____/  \_______/ \_______/ \_______/
//                                                                                                                   | $$                                    
//                                                                                                                   | $$                                    
//                                                                                                                   |__/                                    
