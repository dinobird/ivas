struct Mesh{
    std::vector<vec3>  vertices;
    std::vector<vec2>  texCoords;
    std::vector<vec3>  normals;
    std::vector<ivec3> vertexIndices;
    std::vector<ivec3> texCoordIndices;
    std::vector<ivec3> normalIndices;
};

Mesh loadObj(char const * str){
    Mesh ret;
    for(;*str!='\0';str++){
        if(str[0]=='#'){
            while(*str!='\n') str++;
        }if(str[0]=='v' && str[1]==' '){
            vec3 v;
            int s = sscanf(str,"v %f %f %f",&v.x,&v.y,&v.z);
            if(s==3)
                ret.vertices.push_back(v);
        }else if(str[0]=='v' && str[1]=='t' && str[2]==' '){
            vec2 t;
            if(sscanf(str,"vt %f %f",&t.x,&t.y)==2)
                ret.texCoords.push_back(t);
        }else if(str[0]=='v' && str[1]=='n' && str[2]==' '){
            vec3 n;
            if(sscanf(str,"vn %f %f %f",&n.x,&n.y,&n.z)==3)
                ret.normals.push_back(n);
        }else if(str[0]=='f' && str[1]==' '){
            ivec3 v,n,t;
            if(sscanf(str,"f %d %d %d", &v.x, &v.y, &v.z)==3){
                ret.vertexIndices.push_back(v-ivec3(1,1,1));
            }else if(sscanf(str,"f %d/%d %d/%d %d/%d", 
                        &v.x,&t.x,   &v.y,&t.y,   &v.z,&t.z)==6){
                ret.vertexIndices.push_back(v-ivec3(1,1,1));
                ret.texCoordIndices.push_back(n-ivec3(1,1,1));
            }else if(sscanf(str,"f %d/%d/%d %d/%d/%d %d/%d/%d", 
                        &v.x,&t.x,&n.x,  &v.y,&t.y,&n.y,  &v.z,&t.z,&n.z )==9){
                ret.vertexIndices.push_back(v-ivec3(1,1,1));
                ret.texCoordIndices.push_back(t-ivec3(1,1,1));
                ret.normalIndices.push_back(n-ivec3(1,1,1));
            }else if(sscanf(str,"f %d//%d %d//%d %d//%d", 
                        &v.x,&n.x,  &v.y,&n.y,  &v.z,&n.z)==6){
                ret.vertexIndices.push_back(v-ivec3(1,1,1));
                ret.normalIndices.push_back(n-ivec3(1,1,1));
            }
        }
    }
    return ret;
}
