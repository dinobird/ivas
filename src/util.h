constexpr float pi  = 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862f;

namespace ivas{
template<typename T>
void swap(T& a, T& b){
    T c = a;
    a = b;
    b = c;
}

template<typename T>
T clamp(T a, T lo, T hi){ 
    return a<=lo?lo
         : a>=hi?hi
         : a;
 }

template<typename T>
T min(T a, T b){ return a<b?b:a; }

template<typename T>
T max(T a, T b){ return a>b?b:a; }

template<typename T>
T lerp(T a, T b, float f){ return a+f*(b-a); }

float degrees(float deg){ return deg*(180.f/pi); }

char* mallocTextFile(char const * const filename){
    FILE* file = fopen(filename,"rb");
    if(!file) return NULL;
    fseek(file,0,SEEK_END);
    int size = ftell(file);
    rewind(file);
    auto ret = (char*)malloc(size+1);
    fread(ret,1,size,file);
    ret[size] = '\0';
    fclose(file);
    return ret;
}

}
