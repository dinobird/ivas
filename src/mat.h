float minf(float a, float b){ return a<b?a:b; }
float maxf(float a, float b){ return a>b?a:b; }

float clamp(float f, float min=0, float max=1){
    return f<min ? min
         : f>max ? max
         : f;
}

union mat4
{
	struct {
		float ax, ay, az, aw;
		float bx, by, bz, bw;
		float cx, cy, cz, cw;
		float dx, dy, dz, dw;
	};
	vec4 cols[4];
    float at[16];

	struct {
		alignas(sizeof(vec4))vec3 right;   
		alignas(sizeof(vec4))vec3 forward; 
		alignas(sizeof(vec4))vec3 up;      
		alignas(sizeof(vec4))vec3 position;
	};


	mat4(float ax, float bx, float cx, float dx,
		float ay, float by, float cy, float dy,
		float az, float bz, float cz, float dz,
		float aw, float bw, float cw, float dw
	)
	{
		this->ax = ax; this->bx = bx; this->cx = cx; this->dx = dx;
		this->ay = ay; this->by = by; this->cy = cy; this->dy = dy;
		this->az = az; this->bz = bz; this->cz = cz; this->dz = dz;
		this->aw = aw; this->bw = bw; this->cw = cw; this->dw = dw;
	}

	mat4(vec4 a, vec4 b, vec4 c, vec4 d)
	{
		this->cols[0] = a;
		this->cols[1] = b;
		this->cols[2] = c;
		this->cols[3] = d;
	}

	mat4(float v) {
		*this = mat4(v, 0, 0, 0,
			0, v, 0, 0,
			0, 0, v, 0,
			0, 0, 0, v);
	}

	mat4() {}
};

vec4 operator* (mat4 const& m, vec4 const& v)
{
    auto toReturn = vec4{};
    
    toReturn.x = m.ax*v.x + m.bx*v.y + m.cx*v.z + m.dx*v.w;
    toReturn.y = m.ay*v.x + m.by*v.y + m.cy*v.z + m.dy*v.w;
    toReturn.z = m.az*v.x + m.bz*v.y + m.cz*v.z + m.dz*v.w;
    toReturn.w = m.aw*v.x + m.bw*v.y + m.cw*v.z + m.dw*v.w;

	return toReturn;
}

mat4 operator*(mat4 const& l, mat4 const& r){
	mat4 ret = mat4(0);
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++)
			for(int k=0; k<4; k++)
				ret.cols[j][i] += l.cols[k][i]*r.cols[j][k];
	return ret;
}

void TEST_matrixMultiplication(){
    mat4 l = mat4(1,2,3,4,
                  5,6,7,8,
                  9,10,11,12,
                  13,14,15,16);
    mat4 r = mat4(3,2,1,0,
                  1,2,3,4,
                  6,8,10,12,
                  20,20,30,30);
    mat4 expected = mat4(
        103, 110, 157, 164,
        223, 238, 333, 348,
        343, 366, 509, 532,
        463, 494, 685, 716 );
    mat4 res = l*r;
    for(int i=0; i<16; i++){
        if(res.at[i] != expected.at[i]){
            printf("ERROR: expected %f, got %f\n",expected.at[i], res.at[i]);
        }
    }
    printf("SUCCESS matrixMultiplication\n");
}


union quat {
	struct { float x, y, z, w; };
	struct { vec3 v; };
	float at[4];
	inline void operator*=(quat const& o) {
		x =  x * o.w + y * o.z - z * o.y + w * o.x;
		y = -x * o.z + y * o.w + z * o.x + w * o.y;
		z =  x * o.y - y * o.x + z * o.w + w * o.z;
		w = -x * o.x - y * o.y - z * o.z + w * o.w;
	}
};
inline quat operator*(quat a, quat const& b) { a *= b; return a; }
inline vec3 operator*(quat const& q, vec3 const& v){
    auto xyz = vec3{q.x,q.y,q.z};
    return v + cross(2.f*xyz, cross(xyz,v)+q.w*v);
}
inline quat normalize(quat q){
	auto n = 1.0f/sqrtf(q.x*q.x+q.y*q.y+q.z*q.z+q.w*q.w);
	q.x *= n;
	q.y *= n;
	q.z *= n;
	q.w *= n;
	return q;
}
inline mat4 toMat4(quat const& q) {
	return mat4(
		1.0f - 2.0f*q.y*q.y - 2.0f*q.z*q.z, 2.0f*q.x*q.y - 2.0f*q.z*q.w,        2.0f*q.x*q.z + 2.0f*q.y*q.w,        0.0f,
		2.0f*q.x*q.y + 2.0f*q.z*q.w,        1.0f - 2.0f*q.x*q.x - 2.0f*q.z*q.z, 2.0f*q.y*q.z - 2.0f*q.x*q.w,        0.0f,
		2.0f*q.x*q.z - 2.0f*q.y*q.w,        2.0f*q.y*q.z + 2.0f*q.x*q.w,        1.0f - 2.0f*q.x*q.x - 2.0f*q.y*q.y, 0.0f,
		0.0f,                               0.0f,                               0.0f,                               1.0f);
}

inline quat toQuat(vec3 const& v, float const w) { 
	return quat{ 
		v.x*sinf(w/2.f),
		v.y*sinf(w/2.f), 
		v.z*sinf(w/2.f), 
		cosf(w/2.f)}; 
}

inline quat toQuat(float const pitch, float const roll, float const yaw){
    auto c1 = cosf(roll/2);
    auto s1 = sinf(roll/2);
    auto c2 = cosf(yaw/2);
    auto s2 = sinf(yaw/2);
    auto c3 = cosf(pitch/2);
    auto s3 = sinf(pitch/2);
    auto c1c2 = c1*c2;
    auto s1s2 = s1*s2;
    auto x =c1c2*s3 + s1s2*c3;
    auto y =s1*c2*c3 + c1*s2*s3;
    auto z =c1*s2*c3 - s1*c2*s3;
    auto w =c1c2*c3 - s1s2*s3;
    return quat{x,y,z,w};
}
