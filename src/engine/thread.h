#if defined(TARGET_POSIX)

static void* posix_work(void* v){
    work((uintptr_t)v);
    return NULL;
}
void launch(int threadCount){
    printf("LAUNCHING POSIX THREADS\n");
    // launch worker threads
    for(int i=0; i<threadCount; i++){
        pthread_t thread;
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        // set stack size
        pthread_attr_setstacksize(&attr, fiber_stack_size);
        // lock threads to cores
        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        CPU_SET(i,&cpuset);
        pthread_attr_setaffinity_np(&attr,sizeof(cpuset),&cpuset);  
        // launch the thread
        pthread_create(&thread, &attr, posix_work, (void*)(uintptr_t)i);
        pthread_detach(thread);
        pthread_attr_destroy(&attr);
    }
    // kill this bootstrap thread
    pthread_exit(0);
}

#else
// standard cross platform version
void launch(int threadCount){
    printf("LAUNCHING C++11 THREADS\n");
    std::vector<std::thread> threads;
    for(int i=1; i<threadCount; i++) threads.emplace_back(work,i);
    work(0);
    for(int i=0; i<threadCount-1; i++) threads[i].join();
}
#endif
