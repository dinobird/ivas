struct Arena{
    uint8_t* base;
    int64_t used, cap, high_water_mark;
    std::mutex lock;
};

void initArena(Arena& a, int64_t size){ 
    a.base=(uint8_t*)malloc(size);
    a.used=0;
    a.cap=size;
    a.high_water_mark=0;
}

uint64_t constexpr roundUp(uint64_t const value, uint64_t const multiple){
    assert(multiple && ((multiple & (multiple - 1)) == 0) && "multiple must be positive power of 2");
    return (value + multiple - 1) & -multiple;
}

// alloc() usage example:
//   BigStruct& bigStruct = alloc<BigStruct>(frameArena);
//   Buffer<float> tenFloats = alloc<float>(frameArena,10);
//   Array<float,20>& twentyFloatArray = alloc<float,20>(frameArena);
// if that is not flexible enough, you can use the internal implementation of the convenience functions:
//   uint8_t* customMemoryBlock = alloc(frameArena,1024,16); // allocates 1024 bytes with 16 byte alignment
uint8_t* alloc(Arena &a, int64_t const size, int64_t const align){
    // TODO(douwe): make lock-free (transactional memory style?)
    a.lock.lock();
    a.used = roundUp(a.used, align);
    auto ret = a.base + a.used;
    a.used += size;
    a.high_water_mark = a.used>a.high_water_mark? a.used: a.high_water_mark;
    assert(a.used<=a.cap && "arena reached capacity!");
    a.lock.unlock();
    return ret;
}

template<typename T> T& alloc(Arena &a){ 
    return *(T*)alloc(a,sizeof(T),alignof(T)); 
}

template<typename T> Buffer<T> alloc(Arena &a, int64_t const count){
    auto ptr = (T*)alloc(a,sizeof(T)*count,alignof(T));
    return Buffer<T>{ptr,count};
}

template<typename T, int64_t N> Array<T,N>& alloc(Arena &a){
    return *(Array<T,N>*)alloc(a,sizeof(T)*N,alignof(T));
}
