enum FileStatus{
    invalid = -1,
    busy    =  0,
    ready   =  1
};

#if defined(TARGET_POSIX)
// POSIX async file loading
struct File{
    Buffer<uint8_t> file;
    aiocb aio;
    int64_t open(char const * const filename){
        aio = {};
        aio.aio_fildes = ::open(filename,O_RDONLY);
        if(aio.aio_fildes<=0) return 0;
        struct stat st;
        fstat(aio.aio_fildes,&st);
        aio.aio_nbytes = st.st_size;
        return aio.aio_nbytes;
    }
    void read(Arena &arena){
        file = alloc<uint8_t>(arena,aio.aio_nbytes);
        aio.aio_buf = file.begin(),
        aio_read(&aio);
    }
    void close(){ ::close(aio.aio_fildes); }
    FileStatus status(){ 
        auto e = aio_error(&aio);
        return e==EINPROGRESS? FileStatus::busy
             : e==0? FileStatus::ready 
             : FileStatus::invalid;
    }
    int64_t size(){  return aio.aio_nbytes;}
};

#else
// portable synchronous file loading
struct File{
    Buffer<uint8_t> file;
    FILE* fd;
    int64_t filesize;
    int64_t open(char const * const filename){
        fd = fopen(filename,"rb");
        if(!fd) return 0;
        fseek(fd,0,SEEK_END);
        filesize = ftell(fd);
        fseek(fd,0,SEEK_SET);
        return filesize;
    }
    void read(Arena &arena){
        file = alloc<uint8_t>(arena,size());
        fread(file.begin(),file.size,1,fd);
    }
    void close(){ fclose(fd); }
    FileStatus status(){ return FileStatus::ready; }
    int64_t size(){ return filesize; }
};
#endif

namespace FileCache{
    Arena cache; // TODO: change to evictable allocator
    Stack<Buffer<File>> index;
    Stack<Buffer<char const *>> names;
    void init(int cacheSize, int maxFiles){
        initArena(cache, cacheSize);
        index.data.size = maxFiles;
        index.data.data = (File*)malloc(maxFiles*sizeof(File));
        names.data.size = maxFiles;
        names.data.data = (char const**)malloc(maxFiles*sizeof(char const *));
    }
    FileStatus tryLoad(char const * const filename, Buffer<uint8_t> &out){
        for(int i=0; i<names.size; i++){
            if(strcmp(names[i],filename)==0){
                auto status = index[i].status();
                if(status==FileStatus::ready){
                    out = index[i].file;
                }
                return status;
            }
        }
        File file;
        index.push(file);
        auto size = index.last().open(filename);
        if(size>0){
            index.last().read(cache);
            names.push(filename);
            return index.last().status();
        }else index.pop();
        return FileStatus::invalid;
    }
}

