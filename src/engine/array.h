template<typename T_, int64_t S>
struct Array{
    using T = T_;
    T data[S];
    T      & operator[](int64_t i)       {return data[i];}
    T const& operator[](int64_t i) const {return data[i];}
    T      * begin()       {return &data[0];}
    T const* begin() const {return &data[0];}
    T      * end()       {return &data[S];}
    T const* end() const {return &data[S];}
    int64_t  size() const {return S;}
    constexpr operator bool() const {return true;}
};

template<typename T_>
struct Buffer{
    using T = T_;
    T* data;
    int64_t size;
    T      & operator[](int64_t i)       {return data[i];}
    T const& operator[](int64_t i) const {return data[i];}
    T      * begin()       {return data;}
    T const* begin() const {return data;}
    T      * end()       {return data+size;}
    T const* end() const {return data+size;}
    constexpr operator bool() const {return data!=nullptr;}
};

// usage: Stack<Buffer<T>> or Stack<Array<T>>
template<typename Ts>
struct Stack{
    using T = typename Ts::T;
    Ts data;
    int64_t size;
    T      & operator[](int64_t i)       {return data[i];}
    T const& operator[](int64_t i) const {return data[i];}
    T      * begin()       {return data.begin();}
    T const* begin() const {return data.begin();}
    T      * end()       {return data.end();}
    T const* end() const {return data.end();}
    constexpr operator bool() const {return (bool)data;}
    void push(T t){ data[size++] = t; }
    T pop(){ return data[--size]; }
    void remove(int i){ data[i] = pop(); }
    void stableRemove(int i){ 
        for(size--; i<size; i++)
            data[i] = data[i+1];
    }
    T& last(){return data[size-1];}
};

