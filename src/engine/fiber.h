#include<cstdint>
#include<cstdio>
#include<atomic>
#include<mutex>

struct Fence{
    Fence() = default;
    Fence(std::atomic<int64_t>* a, int64_t v):atom(a),value(v){};
    std::atomic<int64_t>* atom;
    int64_t value;
    bool ready(){
        return !atom || atom->load(std::memory_order::memory_order_consume) >= value;
    }
};

constexpr int64_t fiber_total_size = 1<<10;
constexpr int64_t fiber_stack_size = fiber_total_size - sizeof(Fence);
struct Fiber{
    // BUG(douwe): if fence is switched with data, everything breaks.
    // address of structure is probably used somewhere instead of address of data
    alignas(16) char data[fiber_stack_size];
    Fence fence;
};

struct FiberQueue{
    static constexpr unsigned limit  = 1<<8;
    static constexpr unsigned mask   = limit-1;
    std::mutex lock;
    Fiber fibers[limit] = {};
    unsigned head = 0, tail = 0;
    bool empty(){return head==tail;}
    // DONT FORGET TO UNLOCK AFTER USING THIS FUNCTION
    Fiber* half_push(){
        lock.lock();
        tail = (tail+1)&mask;
        assert(tail!=head && "fibers cap reached!");
        return &fibers[tail];
    }
    void push(Fiber &f){
        *half_push() = f;
        lock.unlock();
    }
    bool pop(Fiber* dest){
        lock.lock();
        if(empty()){
            lock.unlock();
            return false;
        }else{
            head = (head+1)&mask;
            *dest = fibers[head];
            lock.unlock();
            return true;
        }
    }
}queue;

struct Job{struct promise_type;};

namespace std::experimental {
    template <typename R, typename...> struct coroutine_traits {
          using promise_type = typename R::promise_type;
    };
template<typename T> struct coroutine_handle;
template<> struct coroutine_handle<Job::promise_type> {
    Fiber* fiber;
    constexpr coroutine_handle(Fiber* f):fiber(f){}
    constexpr void* address() const noexcept{return (void*)fiber;}
    constexpr static coroutine_handle from_address(void* addr){ return coroutine_handle((Fiber*)addr);}
    constexpr explicit operator bool() const noexcept{return fiber;}
    bool done() const {return __builtin_coro_done(&(fiber->data[0]));}
    void destroy() const {__builtin_coro_destroy(&(fiber->data[0]));}
    void resume() const {__builtin_coro_resume(&(fiber->data[0]));}
    void operator()(){resume();}
};
}

struct Job::promise_type{
    struct WaitForFence{
        WaitForFence(std::atomic<int64_t>* a, int64_t v):fence(Fence(a,v)){ };
        Fence fence;
        bool await_ready(){ return fence.ready(); }
        void await_suspend(std::experimental::coroutine_handle<Job::promise_type> h){ h.fiber->fence = fence; printf("%2d: FENCE SUSPENDED (%lld>=%lld)\n",thread_nr, fence.atom->load(),fence.value); }
        void await_resume(){ printf("%2d: FENCE RESUMED\n",thread_nr); }
    };
    struct InitialSuspend{
        bool await_ready(){ return false; }
        void await_suspend(std::experimental::coroutine_handle<Job::promise_type> h){ 
            queue.lock.unlock();
            h.fiber->fence = Fence(nullptr,0); printf("%2d: INITIAL SUSPENDED\n",thread_nr); }
        void await_resume(){ printf("%2d: INITIAL RESUMED\n",thread_nr); }
    };
    struct FinalSuspend{
        bool await_ready(){ return false; }
        void await_suspend(std::experimental::coroutine_handle<Job::promise_type>){printf("%2d: FINAL SUSPENDED\n",thread_nr);}
        void await_resume(){printf("%2d: FINAL RESUMED\n",thread_nr);}
    };
    InitialSuspend initial_suspend(){ return {}; }
    FinalSuspend   final_suspend(){return {}; }
    void return_void(){}
    Job get_return_object(){ return {}; }
    void unhandled_exception(){}
    void* operator new(size_t n){
        // create on the queue
        assert(n<fiber_stack_size);
        // first half of the push command, unlocked in initialSuspend
        return queue.half_push();
    }
    void operator delete(void*){}
};
using WaitForFence = Job::promise_type::WaitForFence;

void work(int id){
    thread_nr = id;
    Fiber fiber;
    while(queue.pop(&fiber)){
        if(fiber.fence.ready()){
            auto coro = std::experimental::coroutine_handle<Job::promise_type>::from_address((void*)&fiber);
            coro.resume();
            if(!coro.done()){
                queue.push(fiber);
            }
        }else{
            queue.push(fiber);
        }
    }
#ifdef ENABLE_PERF
    for(int i=0; i<perf::cursor; i++){
        printf("%2d %s %s %lld\n",thread_nr, perf::records[i].func, perf::records[i].id, perf::records[i].end - perf::records[i].begin);
    }
#endif
}

