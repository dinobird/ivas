#ifdef ENABLE_PERF
namespace perf{
struct Record{char const *func, *id; uint64_t begin,end;};
thread_local Record records[100];
thread_local int64_t  cursor = 0;

#define STRINGIFY_(X) #X
#define STRINGIFY(X) STRINGIFY_(X)

#define PERF_BEGIN(N)\
    perf::Record &PERF##N=perf::records[perf::cursor++];\
    PERF##N.func = __func__;\
    PERF##N.id=__FILE__ ":" STRINGIFY(__LINE__) ":" #N;\
    _mm_lfence(); PERF##N.begin=__rdtsc(); _mm_lfence();

#define PERF_END(N)\
    _mm_lfence(); PERF##N.end=__rdtsc(); _mm_lfence();

struct PerfBlock{
    Record &r;
    inline PerfBlock(char const * const func, char const * const id):r(records[perf::cursor++]){
        r.func=func; r.id=id;
        _mm_lfence();
        r.begin=__rdtsc(); 
        _mm_lfence();
    }
    inline ~PerfBlock(){
        _mm_lfence();
        r.end=__rdtsc(); 
        _mm_lfence();
    }
};

#define PERF_BLOCK(S) auto PERF_BLOCK##S = perf::PerfBlock(__func__,__FILE__ ":" STRINGIFY(__LINE__) ":" #S);

}
#else

#define PERF_BEGIN(N,S) ;
#define PERF_END(N)     ;
#define PERF_BLOCK(S)   ;

#endif
