
/* 
    CONVENTIONS
    x=right, y=forward, z=up (chirality=right-handed)
    depthbuffer: 1=far, 0=near 
    depth compare: less or equal
    clip space: width=(-1,1), height=(-1,1), depth(0,1);
    winding: counter-clockwise
    matrix order: column-major
*/

#define SIZE(X) (sizeof(X)/sizeof((X)[0]))
#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))

#if defined(__APPLE__) && defined(__MACH__)
#define TARGET_MAC
#endif

#if defined(__linux__) || defined(__linux) || defined(linux)
#define TARGET_LINUX
#endif

#if defined(__unix__) || defined(__unix) || defined(unix) || defined(TARGET_LINUX) || defined(TARGET_MAC)
#define TARGET_POSIX
#endif

#if defined(_WIN32)
#define TARGET_WINDOWS
#endif

using u8  = unsigned char;
using u16 = unsigned short;
using u32 = unsigned int;
using i8  = signed char;
using i16 = short;
using i32 = int;
#if defined(TARGET_WINDOWS)
using u64 = unsigned long long;
using i64 = long long;
#else
using u64 = unsigned long;
using i64 = long;
#endif
using f32 = float;
using f64 = double;

u32 const u32max = 0xFFffFFffU;
i32 const i32max = 0x7FffFFff;
i32 const i32min = 0x80000000;
u64 const u64max = 0xFFFFffffFFFFffffULL;
i64 const i64max = 0x7FFFffffFFFFffffLL;
i64 const i64min = 0x8000000000000000LL;
f32 const inf    = __builtin_inff();

#if defined(TARGET_POSIX)
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<aio.h>
#include<unistd.h>
#include<pthread.h>

#elif defined(TARGET_WINDOWS)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include<windows.h>
#include<xinput.h>
#include"win32_xinput.h"
#undef NOMINMAX
#undef WIN32_LEAN_AND_MEAN
#endif

#if defined(TARGET_WINDOWS)
#define VK_USE_PLATFORM_WIN32_KHR
#elif defined(TARGET_POSIX)
#define VK_USE_PLATFORM_XCB_KHR
#elif defined(TARGET_LINUX)
#define VK_USE_PLATFORM_LINUX_KHR
#elif defined(TARGET_MAC)
#define VK_USE_PLATFORM_MACOS_MVK
#endif


#include<cstring>
#include<chrono>
#include<cassert>
#include<cstdint>
#include<cstdio>
#include<atomic>
#include<mutex>
#include<array>
#include<vector>
#include<thread>
#include<chrono>
#include<cmath>
#include<cstring>
#include<experimental/filesystem>
namespace fs = std::experimental::filesystem;
#include<emmintrin.h>

//#include "imgui.cpp"
//#include "examples/imgui_impl_glfw.cpp"
//#include "examples/imgui_impl_vulkan.cpp"
#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#include "slang/slang.h"

#include "util.h"
#include "engine/array.h"
#include "engine/arena.h"
thread_local int thread_nr = 0;
#define ENABLE_PERF
#include "engine/perf.h"
#include "engine/fiber.h"
#include "engine/thread.h"
#include "engine/file.h"

#include "vec.h"
#include "mat.h"

#include "platform/vulkan.cc"

#include <glslang/Public/ShaderLang.h>
#include "shader.h"
#include "obj.h"


namespace ivas{
    double time = 0.0;

    mat4 projectionMatrix(float aspectRatio, float fovy, float n = 0.1f, float f = 10.f)
    {
        float x = 1.0f/ tanf( 0.5f* fovy);

        mat4 perspective = mat4(
            x/aspectRatio,  0,  0,          0,
            0,              x, 0,          0,
            0,              0,  -f/ (n-f),   (n*f)/(n-f), 
            0,              0,  1,         0
        );
        return perspective*mat4(
            1, 0, 0, 0,
            0, 0,-1, 0,
            0, 1, 0, 0,
            0, 0, 0, 1
        );
    }
};

void resizeWindow(vulkan::RTTest &rt){
    vkQueueWaitIdle(rt.device.computeQueue);
    vkQueueWaitIdle(rt.device.graphicsQueue);
    for(const auto &fb: rt.framebuffers)
        vkDestroyFramebuffer(rt.device.device, fb, NULL);
    for(const auto &iv: rt.swapchain.swapchainImageViews)
        vkDestroyImageView(rt.device.device, iv, NULL);
    vkDestroySwapchainKHR(rt.device.device, rt.swapchain.swapchain, NULL);
    vkDestroySurfaceKHR(rt.instance.instance, rt.surface, NULL);
    vkDestroyImageView(rt.device.device, rt.currentRead.imageView, NULL);
    vkDestroyImageView(rt.device.device, rt.currentWrite.imageView, NULL);
    vmaDestroyImage(rt.device.allocator, rt.currentRead.image, rt.currentRead.allocation);
    vmaDestroyImage(rt.device.allocator, rt.currentWrite.image, rt.currentWrite.allocation);

    rt.surface = vulkan::createVulkanSurface(rt.window, rt.instance.instance);
    rt.swapchain = vulkan::createVulkanSwapchain(rt.window, rt.surface, rt.device, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT, VK_IMAGE_ASPECT_COLOR_BIT);

    VkExtent3D swapchainExtent = {
        .width = rt.swapchain.swapchainCreateInfo.imageExtent.width, 
        .height = rt.swapchain.swapchainCreateInfo.imageExtent.height, 
        .depth = 1
    };
    rt.currentRead  = vulkan::createRTStorageImage(rt.device, swapchainExtent);
    rt.currentWrite = vulkan::createRTStorageImage(rt.device, swapchainExtent);
    vulkan::initRTStorageImgs(rt.device, rt.commandPool, rt.currentRead, rt.currentWrite);

    for(int i=0; i<rt.framebuffers.size(); i++){
        rt.framebuffers[i] = vulkan::createRTFramebuffer(
                rt.device,
                rt.renderPass,
                rt.swapchain.swapchainImageViews[i],
                rt.currentRead.imageView,
                rt.currentWrite.imageView,
                rt.swapchain.swapchainCreateInfo.imageExtent.width,
                rt.swapchain.swapchainCreateInfo.imageExtent.height);
    }
}

void reloadShader(vulkan::RTTest &rt, char const * spvPath){
    vkQueueWaitIdle(rt.device.computeQueue);
    vkQueueWaitIdle(rt.device.graphicsQueue);
    vkDestroyShaderModule(rt.device.device, rt.computeShader.shaderModule,NULL);
    vkDestroyPipeline(rt.device.device, rt.computePipeline.pipeline, NULL);
    loadShaders("src/shaders");
    rt.computeShader   = vulkan::createVulkanShaderModule(rt.device, spvPath, VK_SHADER_STAGE_COMPUTE_BIT);
    rt.computePipeline = vulkan::createRTComputePipeline(rt.device, rt.computeShader, rt.inputDescription);
}

struct Triangle{
    vec3 a; unsigned materialID;
    vec3 b; unsigned _0;
    vec3 c; unsigned _1; 
};

struct Bvh{
    vec3 lo; int count;
    vec3 hi; int leftFirst;
};

vec3 max(vec3 a, vec3 b){ return vec3{MAX(a.x,b.x),MAX(a.y,b.y),MAX(a.z,b.z)}; }
vec3 min(vec3 a, vec3 b){ return vec3{MIN(a.x,b.x),MIN(a.y,b.y),MIN(a.z,b.z)}; }

Bvh generateStupidBvh(Triangle *triangles, int triangleCount){
    Bvh bvh = {
        .lo = vec3{ inf, inf, inf},
        .hi = vec3{-inf,-inf,-inf},
        .count = triangleCount,
        .leftFirst = 0
    };
    for(int i=0; i<triangleCount; i++){
        bvh.lo = min(min(bvh.lo,         triangles[i].a),
                     min(triangles[i].b, triangles[i].c));
        bvh.hi = max(max(bvh.hi,         triangles[i].a),
                     max(triangles[i].b, triangles[i].c));
    }
    return bvh;
}
// pack 3 10-bit indices into a 30-bit Morton code
u32 morton3d(u32 index1, u32 index2, u32 index3){ 
    index1 &= 0x000003ff;
    index2 &= 0x000003ff;
    index3 &= 0x000003ff;
    index1 |= ( index1 << 16 );
    index2 |= ( index2 << 16 );
    index3 |= ( index3 << 16 );
    index1 &= 0x030000ff;
    index2 &= 0x030000ff;
    index3 &= 0x030000ff;
    index1 |= ( index1 << 8 );
    index2 |= ( index2 << 8 );
    index3 |= ( index3 << 8 );
    index1 &= 0x0300f00f;
    index2 &= 0x0300f00f;
    index3 &= 0x0300f00f;
    index1 |= ( index1 << 4 );
    index2 |= ( index2 << 4 );
    index3 |= ( index3 << 4 );
    index1 &= 0x030c30c3;
    index2 &= 0x030c30c3;
    index3 &= 0x030c30c3;
    index1 |= ( index1 << 2 );
    index2 |= ( index2 << 2 );
    index3 |= ( index3 << 2 );
    index1 &= 0x09249249;
    index2 &= 0x09249249;
    index3 &= 0x09249249;
    return( index1 | ( index2 << 1 ) | ( index3 << 2 ) );
}

u32 genMorton(vec3 minBounds, vec3 maxBounds, vec3 centroid){
    u32 x=0,y=0,z=0;
    vec3 scaled = (centroid-minBounds)/(maxBounds-minBounds);
    scaled *= 1023;
    x = (int)scaled.x;
    y = (int)scaled.y;
    z = (int)scaled.z;

    // off-by-one checks
    assert(x>=0 && x<1024);
    assert(y>=0 && y<1024);
    assert(z>=0 && z<1024);

    return morton3d(x,y,z);
}

float surfaceArea(vec3 lo, vec3 hi){
    vec3 ex = (hi-lo);
    float area = 2.f*( ex.x*ex.y + ex.x*ex.z + ex.y*ex.z);
    return area;
}

float surfaceAreaDistance(vec3 loA, vec3 hiA, vec3 loB, vec3 hiB){
    vec3 lo = min(loA,loB);   
    vec3 hi = max(hiA,hiB);
    return surfaceArea(lo,hi);
}

std::vector<Bvh> generateSimpleBvh(Triangle *triangles, int triangleCount, int searchRadius){
    Bvh bounds = generateStupidBvh(triangles,triangleCount);

    struct MortonNode{
        int bvhIdx;
        u32 morton;
        int closestCluster;
    };
    struct EasyBvh{
        int count;
        int right;
        union{ int left; int first; };
        vec3 lo, hi;
    };
    std::vector<EasyBvh> bvh;
    std::vector<MortonNode> clusters;

    // STEP 1) make leaf nodes
    for(int i=0; i<triangleCount; i++){
        Triangle &t = triangles[i];
        bvh.push_back(EasyBvh{
            .lo = min(t.a, min(t.b, t.c)),
            .hi = max(t.a, max(t.b, t.c)),
            .count = 1,
            .first = i,
        });
        clusters.push_back({
            .bvhIdx   = i,
            .morton = genMorton(bounds.lo,
                                bounds.hi,
                                (bvh.back().lo+bvh.back().hi)/2.f)
        });
    }

    // STEP 2) mortonsort
    std::sort(clusters.begin(), clusters.end(), 
        [](MortonNode const& a, MortonNode const& b){return a.morton<b.morton;});

    while(clusters.size()>1){
        // STEP 3) find closest cluster in window
        for(int clusterIdx=0; clusterIdx<clusters.size(); clusterIdx++){
            float closestDist = inf;
            int   closestIdx  = 0;
            for(int i=MAX(clusterIdx-searchRadius, 0);
                    i<MIN(clusterIdx+searchRadius, clusters.size());
                    i++){
                if(i==clusterIdx) continue;
                float dist = surfaceAreaDistance(bvh[clusters[clusterIdx].bvhIdx].lo, 
									                               bvh[clusters[clusterIdx].bvhIdx].hi,
									                               bvh[clusters[i].bvhIdx].lo,
									                               bvh[clusters[i].bvhIdx].hi);
                if(dist<closestDist){
                    closestDist = dist;
                    closestIdx = i;
                }
            }
            clusters[clusterIdx].closestCluster = closestIdx;
        }
        // STEP 4a) find and merge mutual neighbors
        for(int a=0; a<clusters.size(); a++){
            int b = clusters[a].closestCluster;
            if(a == clusters[b].closestCluster){
							  // OPTIMIZE: insert in place instead of at end.
							  // we can then omit the sort (kinda)
                MortonNode left  = clusters[a]; 
                MortonNode right = clusters[b];

                bvh.push_back(EasyBvh{
                    .lo = min(bvh[left.bvhIdx ].lo, 
                              bvh[right.bvhIdx].lo),
                    .hi = max(bvh[left.bvhIdx ].hi, 
                              bvh[right.bvhIdx].hi),
                    .count = 0,
                    .left  = left.bvhIdx,
                    .right = right.bvhIdx
                });

                clusters[b].closestCluster = -2;
                clusters[a] = {
                    .bvhIdx = bvh.size()-1,
                    .morton = genMorton(bounds.lo, bounds.hi, 
                            (bvh.back().lo+bvh.back().hi)/2.f),
                    .closestCluster = -1,
                };
            }
        }
        // STEP 4b) delete marked 
        int offset = 0;
        for(int i=0; i<clusters.size(); i++){
            if(clusters[i].closestCluster==-2){
                offset--;
            }else{
                if(clusters[i].closestCluster!=-1) clusters[i].closestCluster += offset;
                clusters[i+offset] = clusters[i];
            }
        }
        clusters.resize(clusters.size()+offset);
    }

#if 0
    // step 5a) collapse subtrees, mark unused nodes for deletion
    // Note: each triangle can only be in one leaf node
    std::vector<std::vector<int>> triangleIndices;
    triangleIndices.resize(bvh.size());
    for(int n=0; n<bvh.size(); n++)
        if(bvh[n].count>0)
            triangleIndices[n].push_back(bvh[n].first);

    {
        int changes;
        do{
            changes = 0;
            for(int i=0; i<bvh.size(); i++){
                if(bvh[i].count==0){// if branch
                    EasyBvh &branch = bvh[i];
                    EasyBvh &left   = bvh[branch.left];
                    EasyBvh &right  = bvh[branch.right];
                    if(left.count>0 && right.count>0){ // if children are leafs
                        float splitCost = surfaceArea(left.lo,left.hi)*left.count 
                                        + surfaceArea(right.lo,right.hi)*right.count;
                        float stayCost = surfaceArea(branch.lo,branch.hi)*(left.count+right.count);
                        if(splitCost<=stayCost){
                            // do merge
                            branch.count = left.count+right.count;
                            triangleIndices[i].clear();
                            for(int t=0; t<left.count; t++)
                                triangleIndices[i].push_back(triangleIndices[branch.left][t]);
                            for(int t=0; t<right.count; t++)
                                triangleIndices[i].push_back(triangleIndices[branch.right][t]);
                            left.count =-2;
                            right.count=-2;
                            changes++;
                        }
                    }
                }
            }
        }while(changes!=0);
    }

    printf("triangles before transformation:\n");
    for(int i=0; i<triangleCount; i++)
        printf("%3d "
               "(%+1.0f %+1.0f %+1.0f) "
               "(%+1.0f %+1.0f %+1.0f) "
               "(%+1.0f %+1.0f %+1.0f)\n",
               i,
               triangles[i].a.x, triangles[i].a.y, triangles[i].a.z, 
               triangles[i].b.x, triangles[i].b.y, triangles[i].b.z, 
               triangles[i].c.x, triangles[i].c.y, triangles[i].c.z);

    std::vector<Triangle> tmpTriangles;
    tmpTriangles.resize(triangleCount);

    int triangle = 0;
    for(int n=0; n<triangleIndices.size(); n++){
        if(bvh[n].count>0){
            bvh[n].first = triangle;
            for(int i:triangleIndices[n]){
                tmpTriangles[triangle++] = triangles[i];
            }
        }
    }
    for(int i=0; i<triangleCount; i++){
        triangles[i] = tmpTriangles[i];
    }
    
    printf("triangles after transformation:\n");
    for(int i=0; i<triangleCount; i++)
        printf("%3d "
               "(%+1.0f %+1.0f %+1.0f) "
               "(%+1.0f %+1.0f %+1.0f) "
               "(%+1.0f %+1.0f %+1.0f)\n",
               i,
               triangles[i].a.x, triangles[i].a.y, triangles[i].a.z, 
               triangles[i].b.x, triangles[i].b.y, triangles[i].b.z, 
               triangles[i].c.x, triangles[i].c.y, triangles[i].c.z);
#endif
		
    // step 6) walk the tree and emit the nodes in correct form to bvh
    struct Cursor{
        int oldIdx, newIdx;
    };
    std::vector<Cursor> cursors;
    std::vector<Bvh>    ret;

    // push root
		ret.push_back(Bvh{
				.count = 0,
				.leftFirst = 1 ,
        .lo = bvh.back().lo,
        .hi = bvh.back().hi
    });

    // push right of root node
		ret.push_back({});
		ret.push_back({});
    cursors.push_back({bvh.back().left, 1});
    cursors.push_back({bvh.back().right,2});

    while(cursors.size()){
        // pop cursor
        Cursor cursor = cursors.back(); cursors.pop_back();

        EasyBvh const& oldBvh = bvh[cursor.oldIdx];
        Bvh          & newBvh = ret[cursor.newIdx];
        if(oldBvh.count<0) continue; // invalid node created by subtree collapsing

        newBvh.lo    = oldBvh.lo;
        newBvh.hi    = oldBvh.hi;
        newBvh.count = oldBvh.count;
        if(oldBvh.count==0){ // branch
            newBvh.leftFirst = ret.size();
            ret.push_back({}); 
            ret.push_back({}); 
            cursors.push_back({oldBvh.left,  newBvh.leftFirst  });
            cursors.push_back({oldBvh.right, newBvh.leftFirst+1});
        }else{// leaf
            newBvh.leftFirst = oldBvh.first;
        }
    }

    return ret;
}

/*
 ,ggg, ,ggg,_,ggg,                                  
dP""Y8dP""Y88P""Y8b                                 
Yb, `88'  `88'  `88                                 
 `"  88    88    88               gg                
     88    88    88               ""                
     88    88    88    ,gggg,gg   gg    ,ggg,,ggg,  
     88    88    88   dP"  "Y8I   88   ,8" "8P" "8, 
     88    88    88  i8'    ,8I   88   I8   8I   8I 
     88    88    Y8,,d8,   ,d8b,_,88,_,dP   8I   Yb,
     88    88    `Y8P"Y8888P"`Y88P""Y88P'   8I   `Y8                                           
*/

#if defined(TARGET_WINDOWS)
// Windows Entry point for our application
int WINAPI WinMain( 
    HINSTANCE /*hInstance*/,     // HANDLE TO AN INSTANCE.  This is the "handle" to YOUR PROGRAM ITSELF.
    HINSTANCE /*hPrevInstance*/, // USELESS on modern windows (totally ignore hPrevInstance)
    LPSTR     /*szCmdLine*/,     // Command line arguments.  similar to argv in standard C programs
    int       /*iCmdShow*/ 
)
{    // Start window maximized, minimized, etc.
    //auto argc = __argc;
    auto argv = __argv;
#else
int main(int /*argc*/, char** argv){
#endif
    // set working directory to executable directory    
    fs::current_path(fs::canonical(fs::path(argv[0]).parent_path()));

    freopen("ivas_stdout.txt", "w", stdout);
    freopen("ivas_stderr.txt", "w", stderr);

    setbuf(stdout, NULL);
    printf("exe path: %s\n",fs::current_path().u8string().c_str());

    loadShaders("src/shaders");

    if(!glfwInit())
        return -1;

    int width=800, height=600;
    vulkan::RTTest rt = vulkan::initRT(width,height,"src/shaders/test_basic.comp.spv");

    VkSemaphore imageAvailable = vulkan::createVkSemaphore(rt.device);
    VkSemaphore renderingFinished = vulkan::createVkSemaphore(rt.device);
    VkFence completedQueueExecution = vulkan::createVkFence(rt.device);
		
    Triangle triangles[] = {
        // light
        { vec3{-5,-5,5}, 1, vec3{0,5,5}, 1, vec3{5,0,5}, 0},
        // gizmo
        { vec3{0,0,0}, 0, vec3{0,1,0}, 0, vec3{1,0,0}, 0},
        { vec3{0,0,0}, 0, vec3{0,0,1}, 0, vec3{0,1,0}, 0},
        { vec3{0,0,0}, 0, vec3{1,0,0}, 0, vec3{0,0,1}, 0},
        { vec3{1,0,0}, 0, vec3{0,1,0}, 0, vec3{0,0,1}, 0},
        // floor
        { vec3{-9,-9, 0}, 0, vec3{ 9,-9, 0}, 0, vec3{-9, 9, 0}, 0},
        { vec3{ 9, 9, 0}, 0, vec3{-9, 9, 0}, 0, vec3{ 9,-9, 0}, 0},
    };
    std::vector<Bvh> bvh = generateSimpleBvh(&triangles[0], SIZE(triangles), 10);
    int bvhSize = sizeof(Bvh)*bvh.size();
    printf("BVH DEBUG - BVH DEBUG - BVH DEBUG - BVH DEBUG - BVH DEBUG - BVH DEBUG - BVH DEBUG - BVH DEBUG\n");
    for (Bvh const& b : bvh) {
        static int i = 0;
        if (b.count == 0)
            printf("%3d: "
                "(%+1.0f %+1.0f %+1.0f) "
                "(%+1.0f %+1.0f %+1.0f) "
                "left:%d right:%d\n",
                i,
                b.lo.x, b.lo.y, b.lo.z,
                b.hi.x, b.hi.y, b.hi.z,
                b.leftFirst, b.leftFirst + 1);
        else {
            printf("%3d: "
                "(%+1.0f %+1.0f %+1.0f) "
                "(%+1.0f %+1.0f %+1.0f) "
                "leaf (%d + %d)\n",
                i,
                b.lo.x, b.lo.y, b.lo.z,
                b.hi.x, b.hi.y, b.hi.z,
                b.leftFirst, b.count);
        }
        i++;
    }

    vulkan::RTBuffer bvhBuffer = vulkan::createRTBuffer(rt.device, bvhSize, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);
    vulkan::copyWithStagingBuffer(rt.device, rt.commandPool, bvhBuffer.buffer, bvhSize, (void*)&bvh[0]);

    vulkan::RTBuffer triangleBuffer = vulkan::createRTBuffer(rt.device, sizeof(triangles), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);
    vulkan::copyWithStagingBuffer(rt.device, rt.commandPool, triangleBuffer.buffer, sizeof(triangles), (void*)&triangles[0]);

    u32 lightIndices[] = { 0 };
    vulkan::RTBuffer lightIndexBuffer = vulkan::createRTBuffer(rt.device, sizeof(lightIndices), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);
    vulkan::copyWithStagingBuffer(rt.device, rt.commandPool, lightIndexBuffer.buffer, sizeof(lightIndices), (void*)&lightIndices[0]);



    
    u64 timestampQueryResultBuffer[64] = {};
    vulkan::VulkanQueryPool timestampQueryPool = vulkan::createVulkanQueryPool(
        rt.device,
        VK_QUERY_TYPE_TIMESTAMP,
        2,
        0,
        SIZE(timestampQueryResultBuffer),
        sizeof(u64),
        &timestampQueryResultBuffer);

    // starting camera
    vec3 origin = vec3{0,-1,0};
    vec3 forward = vec3{0,1,0};
    float fovy = 1.f;
    
    vulkan::RTComputeShaderPushConstantData pushConstants;
    vec3 right = normalize(cross(forward,vec3{0,0,1}));
    vec3 up = cross(right,forward);
    bool resetCamera = true;

    bool prevEscapeState = false;

    glfwGetWindowSize(rt.window,&width,&height);
    int prevWidth=width, prevHeight=height;
    while(!glfwWindowShouldClose(rt.window)){
        glfwPollEvents();
        glfwGetWindowSize(rt.window,&width,&height);
        if(glfwGetKey(rt.window, GLFW_KEY_DELETE)==GLFW_PRESS){ return 0; }
        if(width==0 || height==0){
            std::this_thread::sleep_for(std::chrono::milliseconds(30)); 
            continue; // skip frame
        }
        
        if(width!=prevWidth || height!=prevHeight){
            printf("resizing: (%d,%d) --> (%d,%d)\n",prevWidth,prevHeight,width,height);
            resizeWindow(rt);
            prevWidth=width;
            prevHeight = height;
            resetCamera=true;
        }

        if(glfwGetKey(rt.window, GLFW_KEY_ESCAPE)==GLFW_PRESS){
            if(!prevEscapeState){ 
                prevEscapeState=true; 
                reloadShader(rt,"src/shaders/test_basic.comp.spv");
                resetCamera = true;
            }
        }else{
            prevEscapeState=false;
        }

        // camera
        float const turnSpeed = 0.001f;
        float const moveSpeed = 0.001f;
        if(glfwGetKey(rt.window, GLFW_KEY_UP   )==GLFW_PRESS && forward.z< 0.95f){ resetCamera=true; forward=normalize(forward+up*turnSpeed);    }
        if(glfwGetKey(rt.window, GLFW_KEY_DOWN )==GLFW_PRESS && forward.z>-0.95f){ resetCamera=true; forward=normalize(forward-up*turnSpeed);    }
        if(glfwGetKey(rt.window, GLFW_KEY_RIGHT)==GLFW_PRESS                    ){ resetCamera=true; forward=normalize(forward+right*turnSpeed); }
        if(glfwGetKey(rt.window, GLFW_KEY_LEFT )==GLFW_PRESS                    ){ resetCamera=true; forward=normalize(forward-right*turnSpeed); }
        if(resetCamera){
            right = normalize(cross(forward,vec3{0,0,1}));
            up    = cross(right,forward);
        }
        if(glfwGetKey(rt.window, GLFW_KEY_D)==GLFW_PRESS){resetCamera=true; origin = origin + right*moveSpeed;}
        if(glfwGetKey(rt.window, GLFW_KEY_A)==GLFW_PRESS){resetCamera=true; origin = origin - right*moveSpeed;}
        if(glfwGetKey(rt.window, GLFW_KEY_W)==GLFW_PRESS){resetCamera=true; origin = origin + forward*moveSpeed;}
        if(glfwGetKey(rt.window, GLFW_KEY_S)==GLFW_PRESS){resetCamera=true; origin = origin - forward*moveSpeed;}
        if(glfwGetKey(rt.window, GLFW_KEY_E)==GLFW_PRESS){resetCamera=true; origin = origin + up*moveSpeed;}
        if(glfwGetKey(rt.window, GLFW_KEY_Q)==GLFW_PRESS){resetCamera=true; origin = origin - up*moveSpeed;}
        if(resetCamera){
            float fovx = fovy*((float)width/(float)height);
            float cotf = cosf(fovy/2.f)/sinf(fovy/2.f);
            pushConstants.bottomLeft  = forward*cotf -right*fovx -up*fovy;
            pushConstants.bottomRight = forward*cotf +right*fovx -up*fovy;
            pushConstants.topLeft     = forward*cotf -right*fovx +up*fovy;
            pushConstants.topRight    = forward*cotf +right*fovx +up*fovy;
            pushConstants.origin_x = origin.x;
            pushConstants.origin_y = origin.y;
            pushConstants.origin_z = origin.z;
            pushConstants.frame=0;
            resetCamera=false;
        }

        vulkan::raytrace(
                rt.device,
                rt.commandPool,     
                rt.swapchain,

                rt.currentRead,
                rt.currentWrite,
                triangleBuffer,
                lightIndexBuffer,
                bvhBuffer,
                rt.inputDescription,
                pushConstants,

                rt.computePipeline,

                imageAvailable,
                renderingFinished,
                completedQueueExecution,

                timestampQueryPool
        );
        pushConstants.frame++; 

        static char titleBuf[1024];
        double gpuTime = (double)(timestampQueryResultBuffer[1] - timestampQueryResultBuffer[0]);
        sprintf(&titleBuf[0], "Ivas(Vulkan RT) - GPU Time: %f ms - FPS : %f", gpuTime / 1'000'000.0, 1'000.0 / (gpuTime / 1'000'000.0));
        glfwSetWindowTitle(rt.window, titleBuf);
    }

    return 1;
}


