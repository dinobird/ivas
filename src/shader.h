enum:uint8_t{
    SHADER_vert,
    SHADER_tesc,
    SHADER_tese,
    SHADER_geom,
    SHADER_frag,
    SHADER_comp,
    SHADER_COUNT
};
enum:uint8_t{
    SHADERBIT_none = 0,
    SHADERBIT_vert = 1<<SHADER_vert,
    SHADERBIT_tesc = 1<<SHADER_tesc,
    SHADERBIT_tese = 1<<SHADER_tese,
    SHADERBIT_geom = 1<<SHADER_geom,
    SHADERBIT_frag = 1<<SHADER_frag,
    SHADERBIT_comp = 1<<SHADER_comp,
    SHADERBIT_MAX  = SHADERBIT_comp
};
char const * const shaderExtension[SHADER_COUNT] = {
    [SHADER_vert]=".vert", 
    [SHADER_tesc]=".tesc",
    [SHADER_tese]=".tese",
    [SHADER_geom]=".geom",
    [SHADER_frag]=".frag",
    [SHADER_comp]=".comp",
};

constexpr int64_t max_filename_length = 63;
struct ProgramFiles{
    char name[max_filename_length];
    uint8_t types;
};

uint8_t extToShaderBit(char const * const str){
    if(strlen(str)!=5) return SHADERBIT_none;
    for(int i=0; i<SHADER_COUNT; i++)
        if(memcmp(str,shaderExtension[i],6)==0) return 1<<i;
    return SHADERBIT_none;
}

std::vector<ProgramFiles> findShaderFiles(char const * const dir){
    std::vector<ProgramFiles> ret;
    for(auto &f: fs::recursive_directory_iterator(dir)){
        if(is_regular_file(f)){
            uint8_t const ext = extToShaderBit(f.path().extension().u8string().c_str());
            if(ext==SHADERBIT_none) continue;
            printf("found %s %s %d\n",
                   f.path().extension().u8string().c_str(),
                   f.path().stem().u8string().c_str(),
                   ext);
            bool found = false;
            for(auto &r:ret){
                if(strncmp(&r.name[0],f.path().stem().u8string().c_str(),max_filename_length)==0){
                    found = true;
                    r.types |= ext;
                }
            }
            if(!found){
                ret.push_back({});
                strncpy(&ret.back().name[0], f.path().stem().u8string().c_str(), max_filename_length);
                ret.back().types = ext;
            }
        }
    }
    return ret;
}

void printShaderFiles(std::vector<ProgramFiles> shaderFiles){
    for(auto &p:shaderFiles){
        printf("%c%c%c%c%c%c %-63s\n",
            p.types&SHADERBIT_vert?'v':'-',
            p.types&SHADERBIT_frag?'f':'-',
            p.types&SHADERBIT_geom?'g':'-',
            p.types&SHADERBIT_tesc?'t':'-',
            p.types&SHADERBIT_tese?'e':'-',
            p.types&SHADERBIT_comp?'c':'-',
            &p.name[0]);
    }
}

void loadShaders(char const * const dir){
retry:
    static char cmd[1<<20];
    static char* cursor;
    cursor = &cmd[0];
    //glslang::InitializeProcess();

    // make path
    char path[256];
    strncpy(&path[0],dir,256-1-max_filename_length-5);
    path[strlen(dir)] = '/';
    char * const affix = &path[strlen(dir)+1];

    auto shaderFiles = findShaderFiles(dir);
    printShaderFiles(shaderFiles);

#if defined(TARGET_WINDOWS)
    for(auto &p:shaderFiles){
        // add filename to path
        strncpy(affix, &p.name[0] ,max_filename_length);
        char *const ext = affix+strnlen(&p.name[0], max_filename_length);

        for(int i=0; i<SHADER_COUNT; i++){
            if(p.types&(1<<i)){
                // add file extension to path
                memcpy(ext, shaderExtension[i], 5);
                printf("path: %.256s\n", &path[0]);

                if(cursor!=&cmd[0]) 
                    cursor += sprintf(cursor," & ");
                cursor += sprintf(cursor,"glslang\\glslangvalidator.exe -V %s -o %s.spv > compile_result.txt", &path[0], &path[0]);
            }
        }
    }
    printf("command: [%s]\n",&cmd[0]);
    if(system(cmd)!=0){
        char const * output = ivas::mallocTextFile("compile_result.txt");
        int value = MessageBoxA(NULL, output, "glsl compilation error", MB_RETRYCANCEL|MB_ICONERROR);
        free((void*)output);
        if(value == IDCANCEL){
            return;
        }else if(value == IDRETRY){
            goto retry;
        }
    }
#endif
    //glslang::FinalizeProcess();
}

