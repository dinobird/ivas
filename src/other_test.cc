#if defined(__APPLE__) && defined(__MACH__)
#define TARGET_MAC
#endif

#if defined(__linux__) || defined(__linux) || defined(linux)
#define TARGET_LINUX
#endif

#if defined(__unix__) || defined(__unix) || defined(unix) || defined(TARGET_LINUX) || defined(TARGET_MAC)
#define TARGET_POSIX
#endif

#if defined(_WIN32)
#define TARGET_WINDOWS
#endif

#include<cassert>
#include<cstdint>
#include<cstdio>
#include<experimental/coroutine>
#include<atomic>
#include<mutex>
#include<vector>
#include<thread>
#include<chrono>
#include<emmintrin.h>

#if defined(TARGET_POSIX)
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<aio.h>
#include<unistd.h>
#include<pthread.h>
#endif

#include "array.h"
#include "arena.h"
thread_local int thread_nr = 0;
#define ENABLE_PERF
#include "perf.h"
#include "fiber.h"
#include "thread.h"
#include "file.h"

std::atomic<int64_t> sync1 = 0;
std::atomic<int64_t> sync2 = 0;

Job test2(int const i){ 
    using namespace std::chrono_literals;
    PERF_BEGIN(begin);
    printf("%2d: test >> %d\n", thread_nr, i);
    PERF_END(begin);
    co_await WaitForFence(&sync2,i);
    PERF_BEGIN(resume);
    sync2++;
    std::this_thread::sleep_for(0.3s);
    printf("%2d: test << %d\n", thread_nr, i);
    PERF_END(resume);
}

Job test(int const i){ 
    using namespace std::chrono_literals;
    {
        PERF_BLOCK(begin)
        printf("%2d: test >> %d\n", thread_nr, i);
    }
    co_await WaitForFence(&sync1,i);
    {
        PERF_BLOCK(resume)
        sync1++;
        test2(i);
        std::this_thread::sleep_for(1s);
        printf("%2d: test << %d\n", thread_nr, i);
    }
}

Job blah()
{
    printf("test yo\n");
    co_await WaitForFence(nullptr, 0);
    co_return;
}

int main(){
    for(int i=100; i>=0; i--) test(rand()%10);
    launch(std::thread::hardware_concurrency());
}

