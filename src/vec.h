#define CI constexpr inline
#define COUNT(T) (sizeof(T)/sizeof(T[0]))

#define V Vec2<T>
template<typename T>
union Vec2 {
	struct { T x, y; };
	T at[2];
    Vec2():x(0),y(0){};
    Vec2(T x_, T y_):x(x_),y(y_){};
	template<typename U> CI explicit operator Vec2<U>(){
		return Vec2<U>(static_cast<U>(x), static_cast<U>(y));
	}
    CI T&   operator[](int64_t i) { return at[i]; }
    CI T const& operator[](int64_t i) const { return at[i]; }
	CI void operator+=(V const o) { x += o.x; y += o.y; }
	CI void operator-=(V const o) { x -= o.x; y -= o.y; }
	CI void operator+=(T const f) { x += f;   y += f;   }
	CI void operator-=(T const f) { x -= f;   y -= f;   }
	CI void operator*=(T const f) { x *= f;   y *= f;   }
	CI void operator/=(T const f) { x /= f;   y /= f;   }
	CI void operator/=(V const o) { x /= o.x; y /= o.y; }
};
#undef V
using  vec2 = Vec2<float>;
using ivec2 = Vec2<int>;
using dvec2 = Vec2<double>;

#define V Vec3<T>
template<typename T>
union Vec3 {
	struct { T x, y, z; };
	struct { T r, g, b; };
	T at[3];
    Vec3():x(0),y(0),z(0){};
    Vec3(T x_, T y_, T z_):x(x_),y(y_),z(z_){};
	template<typename U> CI explicit operator Vec3<U>(){
		return Vec3<U>(static_cast<U>(x), static_cast<U>(y), static_cast<U>(z));
	}
    CI T&   operator[](int64_t i) { return at[i]; }
    CI T const& operator[](int64_t i) const { return at[i]; }
	CI void operator+=(V const o) { x += o.x; y += o.y; z += o.z; }
	CI void operator-=(V const o) { x -= o.x; y -= o.y; z -= o.z; }
	CI void operator+=(T const f) { x += f;   y += f;   z += f;   }
	CI void operator-=(T const f) { x -= f;   y -= f;   z -= f;   }
	CI void operator*=(T const f) { x *= f;   y *= f;   z *= f;   }
	CI void operator/=(T const f) { x /= f;   y /= f;   z /= f;   }
	CI void operator/=(V const o) { x /= o.x; y /= o.y; z /= o.z; }
};
#undef V
using  vec3 = Vec3<float>;
using ivec3 = Vec3<int>;
using dvec3 = Vec3<double>;

#define V Vec4<T>
template<typename T>
union Vec4 {
	struct { T x, y, z, w; };
	struct { T r, g, b, a; };
	T at[4];
    Vec4():x(0),y(0),z(0),w(0){};
    Vec4(T x_, T y_, T z_, T w_):x(x_),y(y_),z(z_),w(w_){};
	template<typename U> CI explicit operator Vec4<U>(){
		return Vec4<U>(static_cast<U>(x), static_cast<U>(y), static_cast<U>(z), static_cast<U>(w));
	}
    CI T&   operator[](int64_t i) { return at[i]; }
    CI T const& operator[](int64_t i) const { return at[i]; }
	CI void operator+=(V const o) { x += o.x; y += o.y; z += o.z; w += o.w; }
	CI void operator-=(V const o) { x -= o.x; y -= o.y; z -= o.z; w -= o.w; }
	CI void operator+=(T const f) { x += f;   y += f;   z += f;   w += f;   }
	CI void operator-=(T const f) { x -= f;   y -= f;   z -= f;   w -= f;   }
	CI void operator*=(T const f) { x *= f;   y *= f;   z *= f;   w *= f;   }
	CI void operator/=(T const f) { x /= f;   y /= f;   z /= f;   w /= f;   }
	CI void operator/=(V const o) { x /= o.x; y /= o.y; z /= o.z; w /= o.w; }
};
#undef V
using  vec4 = Vec4<float>;
using ivec4 = Vec4<int>;
using dvec4 = Vec4<double>;

float skrt(float f){return sqrtf(f);}
double skrt(double d){return sqrt(d);}

#define TT template<typename T> constexpr inline 
#define OPERATORS(V)\
TT V operator+(V a, V b) { a += b; return a; }\
TT V operator+(V a, T f) { a += f; return a; }\
TT V operator+(T f, V a) { a += f; return a; }\
TT V operator-(V a, V b) { a -= b; return a; }\
TT V operator-(V a, T f) { a -= f; return a; }\
TT V operator-(T f, V a) { a -= f; return a; }\
TT V operator*(V a, T f) { a *= f; return a; }\
TT V operator*(T f, V a) { a *= f; return a; }\
TT V operator/(V a, T f) { a /= f; return a; }\
TT V operator/(T f, V a) { a /= f; return a; }\
TT V operator/(V a, V b) { a /= b; return a; }\
TT T dot(V a, V b) { T ret={}; for(int i=0; i<(int)COUNT(a.at); i++) ret+=a.at[i]*b.at[i]; return ret; }\
TT T lengthsq(V a) { return dot(a,a); }\
TT T length(V a) { return skrt(lengthsq(a)); }\
TT T distsq(V a, V b){return lengthsq(b-a);}\
TT T dist(V a, V b){return length(b-a);}\
TT V normalize(V a) { return a/length(a); }

OPERATORS(Vec2<T>)
OPERATORS(Vec3<T>)
OPERATORS(Vec4<T>)

TT Vec3<T> cross(Vec3<T> a, Vec3<T> b){
    return {a.y*b.z - a.z*b.y,
            a.z*b.x - a.x*b.z,
            a.x*b.y - a.y*b.x};
}

#undef CI
#undef TT
