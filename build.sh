clang++ src/ivas.cc -o ivas --std=c++2a -fcoroutines-ts \
    -g -fno-omit-frame-pointer \
    -lpthread -lrt -lstdc++fs\
    -lX11 -lXxf86vm -lXrandr -lXi -ldl \
    -isystem"lib/include/" \
    -Wall -Wextra -Wno-sign-compare -Wno-narrowing -Wno-missing-braces\
    -L"lib/linux64/slang" -lslang \
    -lglfw -lvulkan
