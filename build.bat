clang++ src/ivas.cc -v -o ivas.exe --std=c++2a -fcoroutines-ts --target=x86_64-pc-win32-msvc ^
    -I"lib/include/" ^
    -Wall -Wextra -Wno-sign-compare -Wno-narrowing -Wno-missing-braces -Wno-missing-field-initializers -Wno-deprecated-declarations -Wno-unused-const-variable ^
    -O0 -gfull -gcodeview^
    -Xlinker /subsystem:windows ^
    -L"lib/win64/glfw" -lglfw3dll -lglfw3 ^
    -L"lib/win64/vulkan" -lvulkan-1 ^
    -lUser32 ^
    -fno-omit-frame-pointer ^
    -fms-compatibility
@pause
